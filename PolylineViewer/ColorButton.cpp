#include "stdafx.h"
#include "ColorButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ColorButton

/** @brief ctor
 *
 *  @param[in] color ボタンの色
 */
ColorButton::ColorButton(COLORREF color)
: m_Color(color)
{
}

/** @brief dtor
 */
ColorButton::~ColorButton()
{
}


BEGIN_MESSAGE_MAP(ColorButton, CButton)
	//{{AFX_MSG_MAP(ColorButton)
		// メモ - ClassWizard はこの位置にマッピング用のマクロを追加または削除します。
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ColorButton メッセージ ハンドラ

/** @brief ボタンの色を設定する
 *
 *  @param[in] color ボタンの色
 */
void ColorButton::SetColor(COLORREF color)
{
	m_Color=color;
    if (IsWindow(m_hWnd)) {
    	Invalidate(TRUE);
    }
}

/** @brief ボタンの色を取得する
 */
COLORREF ColorButton::GetColor()
{
	return m_Color;
}

/** @brief コントロールの描画
 *
 *  @param[in] lpDIS DRAWITEMSTRUCTへのポインタ
 */
void ColorButton::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	if (lpDIS->itemAction&(ODA_DRAWENTIRE | ODA_SELECT | ODA_FOCUS)) {
		CString		cs;
		GetWindowText(cs);
		CDC *pDC=CDC::FromHandle(lpDIS->hDC);
		CWnd *pWnd=CWnd::FromHandle(lpDIS->hwndItem);
		CRect rect;
		pWnd->GetClientRect(&rect);
		CBrush brush;
        if (lpDIS->itemState & ODS_DISABLED) {
            brush.CreateHatchBrush(HS_DIAGCROSS, m_Color);
        } else {
            brush.CreateSolidBrush(m_Color);
        }
		CBrush *oldBrush=pDC->SelectObject(&brush);
		pDC->Rectangle(&rect);
		COLORREF oldText=pDC->SetTextColor(m_Color ^ 0x00FFFFFF);
		COLORREF oldBack=pDC->SetBkColor(m_Color);
		pDC->DrawText(cs, &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		DrawFrame(pDC, rect, lpDIS->itemState & ODS_SELECTED);
		pDC->SelectObject(oldBrush);
		pDC->SetTextColor(oldText);
		pDC->SetBkColor(oldBack);
	}
}

/** @brief ボタンの枠を描画
 *
 *  @param[in] pDC CDC
 *  @param[in] rect 枠の矩形
 *  @param[in] bPushed 押されているかどうか
 */
void ColorButton::DrawFrame(CDC* pDC, const CRect& rect, BOOL bPushed)
{
	if (!bPushed) {
		CPen pen1(PS_SOLID, 0, RGB(255, 255, 255));
		CPen *oldPen=pDC->SelectObject(&pen1);
		pDC->MoveTo(rect.right, rect.top);
		pDC->LineTo(rect.left, rect.top);
		pDC->LineTo(rect.left, rect.bottom - 1);
		CPen pen2(PS_SOLID, 0, RGB(0, 0, 0));
		pDC->SelectObject(&pen2);
		pDC->LineTo(rect.right - 1, rect.bottom - 1);
		pDC->LineTo(rect.right - 1, rect.top);
		pDC->SelectObject(oldPen);
	}
	else {
		CPen pen1(PS_SOLID, 0, RGB(0, 0, 0));
		CPen *oldPen=pDC->SelectObject(&pen1);
		pDC->MoveTo(rect.right, rect.top);
		pDC->LineTo(rect.left, rect.top);
		pDC->LineTo(rect.left, rect.bottom - 1);
		CPen pen2(PS_SOLID, 0, RGB(255, 255, 255));
		pDC->SelectObject(&pen2);
		pDC->LineTo(rect.right - 1, rect.bottom - 1);
		pDC->LineTo(rect.right - 1, rect.top);
		pDC->SelectObject(oldPen);
	}
}
