
// PolylineViewer.h : PolylineViewer アプリケーションのメイン ヘッダー ファイル
//
#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"       // メイン シンボル


struct AppSetting;
class ColorTableFile;
class CPolylineViewerView;
class CPolylineViewerDoc;

// CPolylineViewerApp:
// このクラスの実装については、PolylineViewer.cpp を参照してください。
//

typedef void ViewWorkFunction(CView* pView, void* pParam);
typedef void DocumentWorkFunction(CDocument* pDoc, void* pParam);

class CPolylineViewerApp : public CWinAppEx
{
public:
	CPolylineViewerApp();

public:
	void InvalidateAllView();

	void ViewWalk(ViewWorkFunction func, void* pParam);
	void DocumentWalk(DocumentWorkFunction func, void* pParam);

	CPolylineViewerView* GetActivePolylineView() const;
	CPolylineViewerDoc* GetActivePolylineDoc() const;

    std::shared_ptr<AppSetting> GetAppSetting() {return m_pSettting;}
    std::shared_ptr<ColorTableFile> GetColorTable() {return m_pColorTable;}

// オーバーライド
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// 実装
	UINT  m_nAppLook;
	BOOL  m_bHiColorIcons;
    std::shared_ptr<AppSetting> m_pSettting;
    std::shared_ptr<ColorTableFile> m_pColorTable;  ///<色テーブル

	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();

	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnNewPolylineOpen();
    afx_msg void OnUpdateNewPolylineOpen(CCmdUI *pCmdUI);
	afx_msg void OnExistPolylineOpen();
    afx_msg void OnUpdateExistPolylineOpen(CCmdUI *pCmdUI);
    afx_msg void OnAppSetting();
    afx_msg void OnUpdateAppSetting(CCmdUI *pCmdUI);
    afx_msg void OnBmpOutputAll();
    afx_msg void OnUpdateBmpOutputAll(CCmdUI *pCmdUI);
};

extern CPolylineViewerApp theApp;
CPolylineViewerView* GetActivePolylineView();
CPolylineViewerDoc* GetActivePolylineDoc();
