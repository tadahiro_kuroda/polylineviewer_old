#include "stdafx.h"
#include "AboutDlg.h"
#include "Utils.h"

#pragma comment(lib, "Version.lib")

#include <string>
#include <vector>

namespace {
    std::wstring GetCompileData()
    {
        const char* month_name = "JanFebMarAprMayJunJulAugSepOctNovDec";
        int i = 0;
        for (; i < 12; ++i) {
            if (!strncmp(month_name + 3 * i, __DATE__, 3)) {
                break;
            }
        }
        char s[11];
        sprintf_s(s, 11, "%s/%02d/%02d", __DATE__ + 7, i + 1, atoi(__DATE__ + 4));

        std::wstring ws;
        ConvertMBS2Unicode(s, ws);
        return ws;
    }

    bool GetToolNameVerName(HMODULE hModule, CString& sToolName, CString& sVerName)
    {
	    bool	bRet = false;
	    DWORD	dwSize = 0;
	    std::vector<TCHAR>	vtchBufAppPath;
	    std::vector<char> buffByte;

	    dwSize = (2 * _MAX_PATH);
	    vtchBufAppPath.resize(dwSize+2);
	    if (::GetModuleFileName(hModule, vtchBufAppPath.data(), dwSize) == 0)
		    return false;

	    dwSize = ::GetFileVersionInfoSize(vtchBufAppPath.data(), NULL);
	    buffByte.resize(dwSize+2);
	    ::GetFileVersionInfo(vtchBufAppPath.data(), 0, dwSize, buffByte.data());

	    wchar_t* pv;
	    UINT nLen;

	    if (::VerQueryValue((char*)buffByte.data(), _T("\\StringFileInfo\\041104b0\\FileDescription"), (void**)&pv, &nLen))
	    {
		    CString sDesc(pv);
		    sToolName = sDesc;
		    bRet = true;
	    }

	    if (::VerQueryValue((char*)buffByte.data(), _T("\\StringFileInfo\\041104b0\\FileVersion"), (void**)&pv, &nLen))
	    {
		    CString sVersion(pv);
		    sVersion.Replace(_T(","), _T("."));
		    sVersion.Replace(_T(" "), _T(""));
		
		    int nPos;
		    nPos = sVersion.ReverseFind('.');
		    if (::_wtol(sVersion.Mid(nPos + 1)) == 0)
		    {
			    sVersion = sVersion.Left(nPos);
		    }
		    nPos = sVersion.ReverseFind('.');
		    if (::_wtol(sVersion.Mid(nPos + 1)) == 0)
		    {
			    sVersion = sVersion.Left(nPos);
		    }

		    sVerName = sVersion;
	    }

	    return bRet;
    }
}

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_STATIC_VERSION_NUMBER, m_ctlStaticVersion);
    DDX_Control(pDX, IDC_STATIC_VERSION_DATE, m_ctlStaticDate);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    //ツール名称、バージョン
    {
        CString sToolName, sToolVer;    //リソースの名前、バージョン
        GetToolNameVerName(NULL, sToolName, sToolVer);
#ifdef X64
        sToolName += _T("(64bit)");
#elif WIN32
        sToolName += _T("(32bit)");
#else
#error WIN32もしくはX64の定義が必要です 
#endif
        CString sFormat;
        m_ctlStaticVersion.GetWindowText(sFormat);
        CString s;
        s.Format(sFormat, sToolName, sToolVer);
        m_ctlStaticVersion.SetWindowText(s);
    }

    //日付
    {
        CString sFormat;
        m_ctlStaticDate.GetWindowText(sFormat);
        CString s;
        s.Format(sFormat, GetCompileData().c_str());
        //s.Format(sFormat, L"2012/05/21"); //意図的に日付を変えたい場合、こちらを使用
        m_ctlStaticDate.SetWindowText(s);
    }

    return TRUE;  // return TRUE unless you set the focus to a control
    // 例外 : OCX プロパティ ページは必ず FALSE を返します。
}
