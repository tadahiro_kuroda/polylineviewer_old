
// PolylineViewerDoc.cpp : CPolylineViewerDoc クラスの実装
//
#include "stdafx.h"

// SHARED_HANDLERS は、プレビュー、サムネイル、および検索フィルター ハンドラーを実装している ATL プロジェクトで定義でき、
// そのプロジェクトとのドキュメント コードの共有を可能にします。
#ifndef SHARED_HANDLERS
#include "PolylineViewer.h"
#endif

#include "PolylineViewerDoc.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CPolylineViewerDoc

IMPLEMENT_DYNCREATE(CPolylineViewerDoc, CDocument)

BEGIN_MESSAGE_MAP(CPolylineViewerDoc, CDocument)
END_MESSAGE_MAP()


// CPolylineViewerDoc コンストラクション/デストラクション

CPolylineViewerDoc::CPolylineViewerDoc()
{
	// TODO: この位置に 1 度だけ呼ばれる構築用のコードを追加してください。

}

CPolylineViewerDoc::~CPolylineViewerDoc()
{
}

BOOL CPolylineViewerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: この位置に再初期化処理を追加してください。
	// (SDI ドキュメントはこのドキュメントを再利用します。)

	return TRUE;
}




// CPolylineViewerDoc シリアル化

void CPolylineViewerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 格納するコードをここに追加してください。
	}
	else
	{
		// TODO: 読み込むコードをここに追加してください。
	}
}

#ifdef SHARED_HANDLERS

// サムネイルのサポート
void CPolylineViewerDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// このコードを変更してドキュメントのデータを描画します
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// 検索ハンドラーのサポート
void CPolylineViewerDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// ドキュメントのデータから検索コンテンツを設定します。 
	// コンテンツの各部分は ";" で区切る必要があります

	// 例:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CPolylineViewerDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CPolylineViewerDoc 診断

#ifdef _DEBUG
void CPolylineViewerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPolylineViewerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

std::vector<pv::Polyline> CPolylineViewerDoc::s_vExistPolyline;

bool CPolylineViewerDoc::LoadNewRoadPolyline(const std::wstring& path)
{
	return pv::Polyline::Load(path, m_vNewPolyline);
}

bool CPolylineViewerDoc::LoadExistRoadPolyline(const std::wstring& path)
{
	CPolylineViewerDoc::s_vExistPolyline.clear();	// 読み込む度初期化
	return pv::Polyline::Load(path, CPolylineViewerDoc::s_vExistPolyline);
}



// CPolylineViewerDoc コマンド


BOOL CPolylineViewerDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
    if (!CDocument::OnOpenDocument(lpszPathName))
        return FALSE;

    return FALSE;

//    return TRUE;
}


BOOL CPolylineViewerDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
    return FALSE;

    //return CDocument::OnSaveDocument(lpszPathName);
}


void CPolylineViewerDoc::OnCloseDocument()
{
    if (m_bOnlyExistFlag) {
	    s_vExistPolyline.clear();
        theApp.InvalidateAllView();
    }

    CDocument::OnCloseDocument();
}
