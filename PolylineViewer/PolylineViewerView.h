
// PolylineViewerView.h : CPolylineViewerView クラスのインターフェイス
//

#pragma once

#include "Matrix3.h"
#include "Matrix3_.h"

#include "stdafx.h"
#include "afx.h"

#include "PolylineViewerDoc.h"
#include "PolylineViewerView.h"

typedef VM_VECMATH_NS::Matrix3<double> Matrix;

class CPolylineViewerView : public CView
{
protected: // シリアル化からのみ作成します。
	CPolylineViewerView();
	DECLARE_DYNCREATE(CPolylineViewerView)

// 属性
public:
	CPolylineViewerDoc* GetDocument() const;

// 操作
public:
	void Fit();
	void BmpOutput(const std::wstring& sPath);
    
    static void SaveFlag();
    static void LoadFlag();

// オーバーライド
public:
	virtual void OnDraw(CDC* pDC);  // このビューを描画するためにオーバーライドされます。
	void Draw(CDC* pDC);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 実装
public:
	virtual ~CPolylineViewerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:
    void SetMatrix(const Matrix& m) {m_mTransMatrix = m;}
    const Matrix& GetMatrix() const {return m_mTransMatrix;}

protected:
    Matrix m_mTransMatrix;				    ///< 座標の変換行列(x=0〜100、y=0〜100の矩形に描画するためのMatrix) サイズ変更時にこれをViewに貼り付けるように調整する
	static Matrix s_allTransMatrix;         ///< 全View同時操作の際のMatrix
	static bool s_bAllViewMode;             ///< 全View同時操作のフラグ
    static bool s_bShowName;                ///< 道路名称の表示、非表示のフラグ
    static bool s_bShowPoint;               ///< 端点表示、非表示のフラグ

protected:
    bool m_bLButtonDown;                    ///<マウスの左ボタンが押し込まれているか
	CPoint m_ptDown;                        ///<ドラッグ開始位置
	Matrix m_mTransMatrixButtonDown;	    ///< マウス左ボタン押下時の座標の変換行列
    CSize m_sizeOld;                        ///< 以前に描画を行ったサイズ

protected:
	const Matrix& GetTargetMatrix() const{return s_bAllViewMode ? s_allTransMatrix : m_mTransMatrix;}
	Matrix& GetTargetMatrix() {return s_bAllViewMode ? s_allTransMatrix : m_mTransMatrix;}
    Gdiplus::PointF PolylinePointToClient(const pv::Point& pt) const;
    void PolylinePointToClient(const std::vector<pv::Point>& pointsPolyline, std::vector<Gdiplus::PointF>& pointsClient) const;
    pv::Point ClientPointToPolyline(const Gdiplus::PointF& pt) const;
    void ClientPointToPolyline(const std::vector<Gdiplus::PointF>& pointsClient, std::vector<pv::Point>& pointsPolyline) const;
	void ZoomAtViewCenter(double dScale);
	void InvalidateView();
    static Gdiplus::PointF GetPolylineDiahyoPoint(const std::vector<Gdiplus::PointF>& points);

// 生成された、メッセージ割り当て関数
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);

    DECLARE_MESSAGE_MAP()
    afx_msg void OnFit();
    afx_msg void OnUpdateFit(CCmdUI *pCmdUI);
	afx_msg void OnViewZoomIn();
	afx_msg void OnUpdateViewZoomIn(CCmdUI *pCmdUI);
	afx_msg void OnViewZoomOut();
	afx_msg void OnUpdateViewZoomOut(CCmdUI *pCmdUI);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
    afx_msg void OnPan(UINT id);
    afx_msg void OnUpdatePan(CCmdUI *pCmdUI);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnShowHideRoadName();
    afx_msg void OnUpdateShowHideRoadName(CCmdUI *pCmdUI);
    afx_msg void OnFitAllView();
    afx_msg void OnUpdateFitAllView(CCmdUI *pCmdUI);
    afx_msg void OnControlAllView();
    afx_msg void OnUpdateControlAllView(CCmdUI *pCmdUI);
    afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBmpOutput();
    afx_msg void OnShowHidePoint();
    afx_msg void OnUpdateShowHidePoint(CCmdUI *pCmdUI);
};

#ifndef _DEBUG  // PolylineViewerView.cpp のデバッグ バージョン
inline CPolylineViewerDoc* CPolylineViewerView::GetDocument() const
   { return reinterpret_cast<CPolylineViewerDoc*>(m_pDocument); }
#endif

