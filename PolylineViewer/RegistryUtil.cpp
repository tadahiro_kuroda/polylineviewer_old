#include "StdAfx.h"
#include "RegistryUtil.h"



/** @brief レジストリから値を読み込む
 *         pDataは内部でnewを行い、領域の確保を行う。呼び出し側でdelete[]を行うこと
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] type          データのタイプ
 * @param[out] pData        読み込んだ値
 * @param[out] iDataSize    読み込んだデータのサイズ
 * @return 
 */

std::wstring RegistryUtil::m_wsRegistryBase = L"Software\\Zodiac\\PolylineViewer\\";

bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, DWORD type, LPBYTE& pData, DWORD& iDataSize)
{
    std::wstring sSubKey = m_wsRegistryBase;
    sSubKey += szCategory;

    HKEY hKey;
    DWORD result;
    LONG iReturn = RegCreateKeyEx(HKEY_CURRENT_USER, sSubKey.c_str(),
                                  0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS,
                                  NULL, &hKey, &result);
    if (iReturn != ERROR_SUCCESS) {
        _ASSERTE(false);
        return false;
    }
    if (result == REG_CREATED_NEW_KEY) {
        RegCloseKey(hKey);
        return false;
    }

    DWORD dataType = 0;
    RegQueryValueEx(hKey, szName, NULL, &dataType, NULL, &iDataSize);

    if (dataType != type) {
        return false;
    }

    pData = new BYTE[iDataSize];
    RegQueryValueEx(hKey, szName, NULL, NULL, pData, &iDataSize);

    RegCloseKey(hKey);

    return true;
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] type          データのタイプ
 * @param[in] pData         書き込む値
 * @param[in] iDataSize     書き込んだデータのサイズ
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, DWORD type, LPBYTE pData, DWORD iDataSize)
{
    std::wstring sSubKey = m_wsRegistryBase;
    sSubKey += szCategory;

    HKEY hKey;
    DWORD result;
    LONG iReturn = RegCreateKeyEx(HKEY_CURRENT_USER, sSubKey.c_str(),
                                  0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS,
                                  NULL, &hKey, &result);
    if (iReturn != ERROR_SUCCESS) {
        _ASSERTE(false);
        return;
    }

    RegSetValueEx(hKey, szName, 0, type, pData, iDataSize);

    RegCloseKey(hKey);
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] bData        値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, bool& bData)
{
    int i;
    if (!Load(szCategory, szName, i)) {
        return false;
    }
    bData = (i != 0);
    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] iData        値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, int& iData)
{
    LPBYTE pData = NULL;
    DWORD iDataSize = 0;
    if (!Load(szCategory, szName, REG_BINARY, pData, iDataSize)) {
        return false;
    }

    iData = *((int*)pData);
    delete[] pData;
    pData = NULL;

    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] iData        値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, UINT& iData)
{
    LPBYTE pData = NULL;
    DWORD iDataSize = 0;
    if (!Load(szCategory, szName, REG_BINARY, pData, iDataSize)) {
        return false;
    }

    iData = *((int*)pData);
    delete[] pData;
    pData = NULL;

    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] iData        値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, DWORD& iData)
{
    LPBYTE pData = NULL;
    DWORD iDataSize = 0;
    if (!Load(szCategory, szName, REG_BINARY, pData, iDataSize)) {
        return false;
    }

    iData = *((DWORD*)pData);
    delete[] pData;
    pData = NULL;

    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] iData        値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, long& iData)
{
    LPBYTE pData = NULL;
    DWORD iDataSize = 0;
    if (!Load(szCategory, szName, REG_BINARY, pData, iDataSize)) {
        return false;
    }

    iData = *((long*)pData);
    delete[] pData;
    pData = NULL;

    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] dData        値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, double& dData)
{
    LPBYTE pData = NULL;
    DWORD iDataSize = 0;
    if (!Load(szCategory, szName, REG_BINARY, pData, iDataSize)) {
        return false;
    }

    dData = *((double*)pData);
    delete[] pData;
    pData = NULL;

    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] sData        値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, CString& sData)
{
    LPBYTE pData = NULL;
    DWORD iDataSize = 0;
    if (!Load(szCategory, szName, REG_SZ, pData, iDataSize)) {
        return false;
    }

    sData = (TCHAR*)pData;
    delete[] pData;
    pData = NULL;

    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] sData        値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, std::wstring& sData)
{
    LPBYTE pData = NULL;
    DWORD iDataSize = 0;
    if (!Load(szCategory, szName, REG_SZ, pData, iDataSize)) {
        return false;
    }

    sData = (TCHAR*)pData;
    delete[] pData;
    pData = NULL;

    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] listData     値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, std::list<int>& listData)
{
    std::list<std::wstring> listStr;
    if (!Load(szCategory, szName, listStr)) {
        return false;
    }
    listData.clear();
    std::list<std::wstring>::const_iterator it = listStr.begin();
    for (; it != listStr.end(); ++it) {
        listData.push_back(_tstoi(it->c_str()));
    }
    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] listData     値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, std::list<std::wstring>& listData)
{
    listData.clear();
    LPBYTE pData = NULL;
    DWORD iDataSize = 0;
    if (!Load(szCategory, szName, REG_MULTI_SZ, pData, iDataSize)) {
        return false;
    }

    size_t iCount = 0;
    TCHAR* pChar = (TCHAR*)pData;
    while (iCount + sizeof(TCHAR) < iDataSize) {
        if (*pChar == NULL) {
            break;
        }
        listData.push_back(pChar);
        size_t iCharCount = _tcslen(pChar) + 1; //+1はNULL文字
        pChar += iCharCount;
        iCount += iCharCount * sizeof(TCHAR);
    }

    delete[] pData;
    pData = NULL;

    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] setData      値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, std::set<int>& setData)
{
    std::list<std::wstring> listStr;
    if (!Load(szCategory, szName, listStr)) {
        return false;
    }
    setData.clear();
    std::list<std::wstring>::const_iterator it = listStr.begin();
    for (; it != listStr.end(); ++it) {
        setData.insert(_tstoi(it->c_str()));
    }
    return true;
}

/** @brief レジストリから値を読み込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[out] arrayData    値
 * @return 
 */
bool RegistryUtil::Load(LPCTSTR szCategory, LPCTSTR szName, std::vector<std::wstring>& arrayData)
{
    arrayData.clear();
    LPBYTE pData = NULL;
    DWORD iDataSize = 0;
    if (!Load(szCategory, szName, REG_MULTI_SZ, pData, iDataSize)) {
        return false;
    }

    size_t iCount = 0;
    TCHAR* pChar = (TCHAR*)pData;
    while (iCount + sizeof(TCHAR) < iDataSize) {
        if (*pChar == NULL) {
            break;
        }
        arrayData.push_back(pChar);
        size_t iCharCount = _tcslen(pChar) + 1; //+1はNULL文字
        pChar += iCharCount;
        iCount += iCharCount * sizeof(TCHAR);
    }

    delete[] pData;
    pData = NULL;

    return true;
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] bData         値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, bool bData)
{
    int i = bData ? 1 : 0;
    Save(szCategory, szName, i);
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] iData         値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, int iData)
{
    Save(szCategory, szName, REG_BINARY, (LPBYTE)(&iData), sizeof(int));
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] iData         値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, UINT iData)
{
    Save(szCategory, szName, REG_BINARY, (LPBYTE)(&iData), sizeof(iData));
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] iData         値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, long iData)
{
    Save(szCategory, szName, REG_BINARY, (LPBYTE)(&iData), sizeof(long));
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] iData         値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, DWORD iData)
{
    Save(szCategory, szName, REG_BINARY, (LPBYTE)(&iData), sizeof(iData));
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] dData         値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, double dData)
{
    Save(szCategory, szName, REG_BINARY, (LPBYTE)(&dData), sizeof(double));
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] sData         値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, const CString& sData)
{
    std::wstring s = (LPCTSTR)sData;
    Save(szCategory, szName, s);
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] sData         値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, const std::wstring& sData)
{
    DWORD iCount = (DWORD)((sData.size() + 1) * sizeof(TCHAR));
    Save(szCategory, szName, REG_SZ, (LPBYTE)sData.c_str(), iCount);
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] listData      値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, const std::list<int>& listData)
{
    std::list<std::wstring> listStr;
    std::list<int>::const_iterator it = listData.begin();
    for (; it != listData.end(); ++it) {
        CString s;
        s.Format(_T("%d"), *it);
        listStr.push_back(std::wstring(static_cast<const wchar_t*>(s)));
    }
    Save(szCategory, szName, listStr);
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] listData      値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, const std::list<std::wstring>& listData)
{
    size_t iCountChar = 0;
    std::list<std::wstring>::const_iterator it = listData.begin();
    for (; it != listData.end(); it++) {
        iCountChar += it->size() + 1;   //+1はNULL文字分
    }
    iCountChar += 1;    //最後はNULL2文字で終る
    TCHAR* pChar = new TCHAR[iCountChar];

    it = listData.begin();
    size_t iCountTemp = 0;
    for (; it != listData.end(); it++) {
		//_tcscpy(pChar + iCountTemp, it->c_str());	// エラーが出たので以下に変更
        _tcscpy_s(pChar + iCountTemp, iCountChar+iCountTemp, it->c_str());
        iCountTemp += it->size() + 1;
    }
    pChar[iCountTemp] = 0;

    Save(szCategory, szName, REG_MULTI_SZ, (LPBYTE)pChar, (DWORD)(iCountChar * sizeof(TCHAR)));

    delete[] pChar;
    pChar = NULL;
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] setData       値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, const std::set<int>& setData)
{
    std::list<std::wstring> listStr;
    std::set<int>::const_iterator it = setData.begin();
    for (; it != setData.end(); ++it) {
        CString s;
        s.Format(_T("%d"), *it);
        listStr.push_back(std::wstring(static_cast<const wchar_t*>(s)));
    }
    Save(szCategory, szName, listStr);
}
/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] arrayData         値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, const std::vector<std::wstring>& arrayData)
{
    size_t iCountChar = 0;
    std::vector<std::wstring>::const_iterator it = arrayData.begin();
    for (; it != arrayData.end(); it++) {
        iCountChar += it->size() + 1;   //+1はNULL文字分
    }
    iCountChar += 1;    //最後はNULL2文字で終る
    TCHAR* pChar = new TCHAR[iCountChar];

    it = arrayData.begin();
    size_t iCountTemp = 0;
    for (; it != arrayData.end(); it++) {
        //_tcscpy(pChar + iCountTemp, it->c_str()); // エラーが出たので以下に変更
		_tcscpy_s(pChar + iCountTemp, iCountChar+iCountTemp, it->c_str());
        iCountTemp += it->size() + 1;
    }
    pChar[iCountTemp] = 0;

    Save(szCategory, szName, REG_MULTI_SZ, (LPBYTE)pChar, (DWORD)(iCountChar * sizeof(TCHAR)));

    delete[] pChar;
    pChar = NULL;
}

/** @brief レジストリへ値を書き込む
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 * @param[in] sData         値
 */
void RegistryUtil::Save(LPCTSTR szCategory, LPCTSTR szName, LPCTSTR sData)
{
    std::wstring s = sData;
    Save(szCategory, szName, s);
}

/** @brief レジストリの値を削除
 *
 * @param[in] szCategory    カテゴリ
 * @param[in] szName        名称
 */
void RegistryUtil::Clear(LPCTSTR szCategory, LPCTSTR szName)
{
    std::wstring sSubKey = m_wsRegistryBase;
    sSubKey += szCategory;

    HKEY hKey;
    DWORD result;
    LONG iReturn = RegCreateKeyEx(HKEY_CURRENT_USER, sSubKey.c_str(),
                                  0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS,
                                  NULL, &hKey, &result);
    if (iReturn != ERROR_SUCCESS) {
        _ASSERTE(false);
        return;
    }
    if (result == REG_CREATED_NEW_KEY) {
        RegCloseKey(hKey);
        return;
    }

    RegDeleteValue(hKey, szName);

    RegCloseKey(hKey);

    return;
}

/** @brief レジストリの値をカテゴリごと削除
 *
 * @param[in] szCategory    カテゴリ
 */
void RegistryUtil::Clear(LPCTSTR szCategory)
{
    std::wstring sSubKey = m_wsRegistryBase;
    sSubKey += szCategory;

    RegDeleteKey(HKEY_CURRENT_USER, sSubKey.c_str());
}


/** @brief レジストリのベースディレクトリを設定
 *
 * @param[in] base    ベースディレクトリ
 */
void RegistryUtil::SetBase(std::wstring& base)
{
    m_wsRegistryBase = base;
}


/** @brief レジストリのベースディレクトリを取得
 */
std::wstring RegistryUtil::GetBase()
{
    return m_wsRegistryBase;
}

