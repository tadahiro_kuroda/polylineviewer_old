//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ で生成されたインクルード ファイル。
// PolylineViewer.rc で使用
//
#define IDD_ABOUTBOX                    100
#define ID_STATUSBAR_PANE1              120
#define ID_STATUSBAR_PANE2              121
#define IDS_STATUS_PANE1                122
#define IDS_STATUS_PANE2                123
#define IDS_TOOLBAR_STANDARD            124
#define IDS_TOOLBAR_CUSTOMIZE           125
#define ID_VIEW_CUSTOMIZE               126
#define IDR_MAINFRAME                   128
#define IDR_MAINFRAME_256               129
#define IDR_PolylineViewerTYPE          130
#define ID_WINDOW_MANAGER               131
#define IDS_PROPERTIES_WND              158
#define ID_VIEW_APPLOOK_WIN_2000        205
#define ID_VIEW_APPLOOK_OFF_XP          206
#define ID_VIEW_APPLOOK_WIN_XP          207
#define ID_VIEW_APPLOOK_OFF_2003        208
#define ID_VIEW_APPLOOK_VS_2005         209
#define ID_VIEW_APPLOOK_VS_2008         210
#define ID_VIEW_APPLOOK_OFF_2007_BLUE   215
#define ID_VIEW_APPLOOK_OFF_2007_BLACK  216
#define ID_VIEW_APPLOOK_OFF_2007_SILVER 217
#define ID_VIEW_APPLOOK_OFF_2007_AQUA   218
#define ID_VIEW_APPLOOK_WINDOWS_7       219
#define IDS_EDIT_MENU                   306
#define IDS_ERROR_RANDOM_COLOR_FILE     307
#define IDD_APP_SETTING                 313
#define IDC_COLOR_BUTTON_NEW_POLYLINE   1005
#define IDC_EDIT_WIDTH_NEW_POLYLINE     1006
#define IDC_COLOR_BUTTON_EXIST_POLYLINE 1009
#define IDC_EDIT_WIDTH_EXIST_POLYLINE   1010
#define IDC_STATIC_VERSION_NUMBER       1022
#define IDC_STATIC_VERSION_DATE         1023
#define IDC_CHECK_RANDOM_COLOR          1024
#define IDC_MFCEDITBROWSE_RANDOM_COLOR  1026
#define ID_NEW_POLYLINE_OPEN            32774
#define ID_EXIST_POLYLINE_OPEN          32775
#define ID_APP_SETTING                  32780
#define ID_FIT                          32782
#define ID_VIEW_ZOOM_OUT                32787
#define ID_VIEW_ZOOM_IN                 32788
#define ID_EXPANSION                    32789
#define ID_REDUCTION                    32791
#define ID_FIT_ALL_VIEW                 32812
#define ID_CONTROL_ALL_VIEW             32813
#define ID_1PX_UP                       32825
#define ID_1PX_DOWN                     32826
#define ID_1PX_LEFT                     32827
#define ID_1PX_RIGHT                    32828
#define ID_10PX_UP                      32829
#define ID_10PX_DOWN                    32830
#define ID_10PX_LEFT                    32831
#define ID_10PX_RIGHT                   32832
#define ID_100PX_UP                     32833
#define ID_100PX_DOWN                   32834
#define ID_100PX_LEFT                   32835
#define ID_100PX_RIGHT                  32836
#define ID_SCREEN_SIZE_UP               32837
#define ID_SCREEN_SIZE_DOWN             32838
#define ID_SCREEN_SIZE_LEFT             32839
#define ID_SCREEN_SIZE_RIGHT            32840
#define ID_SHOW_HIDE_ROAD_NAME          32846
#define ID_BMP_OUTPUT                   32849
#define ID_BMP_OUTPUT_ALL               32850
#define ID_SHOW_HIDE_POINT              32853

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        318
#define _APS_NEXT_COMMAND_VALUE         32854
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
