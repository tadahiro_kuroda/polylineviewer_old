#include "stdafx.h"
#include ".\utils.h"

#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <locale>

/** SJISからUnicodeに変換
    @param[in] s : SJIS文字列
    @param[out] sResult : 変換されたUnicode文字列
    @return 成功したとき true */
bool ConvertMBS2Unicode(const std::string& s, std::wstring& sResult)
{
    int nBufSize = MultiByteToWideChar(CP_ACP, 0, s.c_str(), -1, NULL, 0);
    std::vector<wchar_t> vBuf(nBufSize); 
    if (MultiByteToWideChar(CP_ACP, 0, s.c_str(), -1, &vBuf[0], nBufSize) == 0) {
        ASSERT(false);
        return false;
    }
    sResult = std::wstring(&vBuf[0]);
    return true;
}

/** UnicodeからSJISに変換
    @param[in] s : Unicode文字列
    @param[out] sResult : 変換されたSJIS文字列
    @return 成功したとき true */
bool ConvertUnicode2MBS(const std::wstring& s, std::string& sResult)
{
    int nBufSize = WideCharToMultiByte(CP_ACP, 0, s.c_str(), -1, NULL, 0, NULL, NULL);
    std::vector<char> vBuf(nBufSize); 
    if (WideCharToMultiByte(CP_ACP, 0, s.c_str(), -1, &vBuf[0], nBufSize, NULL, NULL) == 0) {
        ASSERT(false);
        return false;
    }
    sResult = std::string(&vBuf[0]);
    return true;
}

/** @brief UTF8文字列からワイド文字列に変換する
 *
 *  @param[in] s UTF8文字列
 *  @param[out] sResult sをワイド文字列に変換したもの
 *  @retval true 正常終了した
 *  @retval false 変換できなかった
 */
bool ConvertUTF82Unicode(const std::string& s, std::wstring& sResult)
{
    int nBufSize = MultiByteToWideChar(CP_UTF8, 0, s.c_str(), -1, NULL, 0);
    std::vector<wchar_t> vBuf(nBufSize); 
    if (MultiByteToWideChar(CP_UTF8, 0, s.c_str(), -1, &vBuf[0], nBufSize) == 0) {
        _ASSERTE(false);
        return false;
    }
    sResult = std::wstring(&vBuf[0]);
    return true;
}

/** @brief ワイド文字列からUTF8文字列に変換する
 *
 *  @param[in] s ワイド文字列
 *  @param[out] sResult sをにUTF8文字列変換したもの
 *  @retval true 正常終了した
 *  @retval false 変換できなかった
 */
bool ConvertUnicode2UTF8(const std::wstring& s, std::string& sResult)
{
    int nBufSize = WideCharToMultiByte(CP_UTF8, 0, s.c_str(), -1, NULL, 0, NULL, NULL);
    std::vector<char> vBuf(nBufSize); 
    if (WideCharToMultiByte(CP_UTF8, 0, s.c_str(), -1, &vBuf[0], nBufSize, NULL, NULL) == 0) {
        _ASSERTE(false);
        return false;
    }
    sResult = std::string(&vBuf[0]);
    return true;
}

bool ConvertJisToSJis(const std::string& jis, std::string& sjis)
{
    sjis.clear();

    size_t nJis = jis.length();

    size_t i = 0;
    bool bConvert = false;    //JIS->SJIS変換が必要かどうか

    while (i < nJis) {
        if (i < nJis - 3) {
            if (jis[i] == 0x1b && jis[i+1] == 0x24 && jis[i+2] == 0x42) {
                bConvert = true;
                i += 3;
                continue;
            }
            if (jis[i] == 0x1b && jis[i+1] == 0x28 && jis[i+2] == 0x42) {
                bConvert = false;
                i += 3;
                continue;
            }
        }
        if (i < nJis - 2) {
            if (bConvert && 0x21 <= jis[i] && jis[i] <= 0x7e) {
                WORD w = MAKEWORD(jis[i+1], jis[i]);
                unsigned int c = _mbcjistojms(w);
                if (c == 0) {
                    return false;
                }
                sjis.push_back((char)HIBYTE(c));
                sjis.push_back((char)LOBYTE(c));
                i += 2;
                continue;
            }
        }
        if (jis[i] == '\0') {
            break;
        }
        sjis.push_back(jis[i]);
        i++;
    }
    return true;
}

bool ConvertSJisToJis(const std::string& sjis, std::string& jis)
{
    size_t nSjis = sjis.length();
    size_t i = 0;
    bool bConvert = false;

    while (i < nSjis) {
        if (i < nSjis - 2) {
            if (_ismbblead(sjis[i]) && !bConvert) {
                jis.push_back(0x1b);
                jis.push_back(0x24);
                jis.push_back(0x42);
                WORD w = MAKEWORD(sjis[i + 1], sjis[i]);
                unsigned int c = _mbcjmstojis(w);
                if (c == 0) {
                    return false;
                }
                jis.push_back((char)HIBYTE(c));
                jis.push_back((char)LOBYTE(c));
                bConvert = true;
                i += 2;
                if (!_ismbblead(sjis[i])) {
                    jis.push_back(0x1b);
                    jis.push_back(0x28);
                    jis.push_back(0x42);
                    bConvert = false;
                }
                continue;
            }
            if (_ismbblead(sjis[i]) && bConvert) {
                WORD w = MAKEWORD(sjis[i + 1], sjis[i]);
                unsigned int c = _mbcjmstojis(w);
                if (c == 0) {
                    return false;
                }
                jis.push_back((char)HIBYTE(c));
                jis.push_back((char)LOBYTE(c));
                i += 2;
                if (!_ismbblead(sjis[i])) {
                    jis.push_back(0x1b);
                    jis.push_back(0x28);
                    jis.push_back(0x42);
                    bConvert = false;
                }
                continue;
            }
        }
        if (sjis[i] == '\0') {
            break;
        }
        jis.push_back(sjis[i]);
        i++;
    }
    return true;
}

/** @brief ファイルが存在するかを調べる
 *         以前はディレクトリがある場合も必ずtrueを返していた、後でFileだけをチェックするフラグをつけたので、デフォルトでディレクトリでもOKとしている
 *
 *  @param[in] szFile 調べるファイルのフルパス
 *  @param[in] bExistDirCheck trueの場合存在するのがフォルダでもOKとする
 *  @return 存在する場合 true */
bool IsFileExist(const wchar_t* szFile, bool bExistDirCheck /*= true*/)
{
    WIN32_FIND_DATAW  wfd;

    HANDLE hFile = FindFirstFileW(szFile, &wfd);
    bool bResult = (hFile != INVALID_HANDLE_VALUE);
    FindClose(hFile);

    if (!bResult || bExistDirCheck) {
        return bResult;
    }

    DWORD attr = ::GetFileAttributesW(szFile);
    if (attr == (DWORD)-1) {
        return false;
    }

    return !(attr & FILE_ATTRIBUTE_DIRECTORY);
}

/** @brief ファイルが存在するかを調べる
 *         以前はディレクトリがある場合も必ずtrueを返していた、後でFileだけをチェックするフラグをつけたので、デフォルトでディレクトリでもOKとしている
 *
 *  @param[in] szFile 調べるファイルのフルパス
 *  @param[in] bExistDirCheck trueの場合存在するのがフォルダでもOKとする
 *  @return 存在する場合 true */
bool IsFileExist(const char* szFile, bool bExistDirCheck /*= true*/)
{
    WIN32_FIND_DATAA  wfd;

    HANDLE hFile = FindFirstFileA(szFile, &wfd);
    bool bResult = (hFile != INVALID_HANDLE_VALUE);
    FindClose(hFile);

    if (!bResult || bExistDirCheck) {
        return bResult;
    }

    DWORD attr = ::GetFileAttributesA(szFile);
    if (attr == (DWORD)-1) {
        return false;
    }

    return !(attr & FILE_ATTRIBUTE_DIRECTORY);
}

bool IsFileExist(const std::string& sFile, bool bExistDirCheck /*= true*/)
{
    return IsFileExist(sFile.c_str(), bExistDirCheck);
}

bool IsFileExist(const std::wstring& sFile, bool bExistDirCheck /*= true*/)
{
    return IsFileExist(sFile.c_str(), bExistDirCheck);
}

inline bool IsSeparateChar(wchar_t c)
{
    return ((c)==L'\\' || (c)==L'/');
}

/** フルパス名からファイル名とディレクトリのフルパス名とに分解する
    @param[in] sFileNameWithPath フルパス文字列
    @param[out] sDir ディレクトリのフルパス名(末尾は'\')
    @param[out] sFile ファイル名
    @return 成功したとき true */
void SplitFileName(const std::wstring& sFileNameWithPath, std::wstring& sDir, std::wstring& sFile)
{
    unsigned long npos = 0;
    unsigned long nLastSeparator = 0;
    while (npos < sFileNameWithPath.size()) {
        if (IsSeparateChar(sFileNameWithPath[npos])) {
            nLastSeparator = npos;
        }
        ++npos;
    }

    if (nLastSeparator != 0) {
        std::wstring s = sFileNameWithPath;
        sDir = s.substr(0, nLastSeparator + 1);
        sFile = s.substr(nLastSeparator + 1);
    } else{
        sDir.clear();
        sFile = sFileNameWithPath;
    }
}

inline bool IsSeparateChar(char c)
{
    //return ((c)=='\\' || (c)=='/' || (c)==':');
    return ((c)=='\\' || (c)=='/');
}

/** フルパス名からファイル名とディレクトリのフルパス名とに分解する
    @param[in] sFileNameWithPath フルパス文字列
    @param[out] sDir ディレクトリのフルパス名(末尾は'\')
    @param[out] sFile ファイル名
    @return 成功したとき true */
void SplitFileName(const std::string& sFileNameWithPath, std::string& sDir, std::string& sFile)
{
    unsigned long npos = 0;
    unsigned long nLastSeparator = 0;
    while(npos < sFileNameWithPath.size()){
        if( IsSeparateChar(sFileNameWithPath[npos]) ){
            nLastSeparator = npos;
            ++npos;
        } else if( IsTwoBytesChar(sFileNameWithPath[npos]) ){
            npos += 2;
        } else{
            ++npos;
        }
    }

    if (nLastSeparator != 0) {
        std::string s = sFileNameWithPath;
        sDir = s.substr(0, nLastSeparator + 1);
        sFile = s.substr(nLastSeparator + 1);
    } else{
        sDir.clear();
        sFile = sFileNameWithPath;
    }
}

inline bool IsPeriodChar(wchar_t c)
{
    return ((c)==L'.');
}

/** @brief ファイル名から拡張子とそれ以外とに分解する
    @note 拡張子が存在しないとき sExt は空になる
    @param[in] sFileName ファイル名
    @param[out] sBody 拡張子以外の部分
    @param[out] sExt 拡張子
    @return 成功したとき true */
bool SplitFileExtension(const std::wstring& sFileName, std::wstring& sBody, std::wstring& sExt)
{
    unsigned long npos = 0;
    unsigned long nLastSeparator = 0;
    while (npos < sFileName.size()) {
        if (IsPeriodChar(sFileName[npos])) {
            nLastSeparator = npos;
        }
        ++npos;
    }

    if (nLastSeparator != 0) {
        sBody = sFileName.substr(0, nLastSeparator);
        sExt = sFileName.substr(nLastSeparator + 1);
    }
    else{
        sBody = sFileName;
        sExt.clear();
    }

    return true;
}

/** @brief ディレクトリを作成(中間のディレクトリも無ければ作成される)
 *
 * @param[in]   sDir                作成するディレクトリ
 * @param[out]  psCannotCreateDir   正しく作成されなかったディレクトリ
 * @return 正しく作成された場合、true
 */
bool CreateDirectories(const std::wstring& sDir, std::wstring* psCannotCreateDir /*= NULL*/)
{
    if (sDir.size() > FILENAME_MAX) {
        _ASSERTE(false);
    }

    std::wstring sTemp = sDir;

    //Pathの分割
    std::list<std::wstring> listPath;
    while (sTemp.size() > 2) {    //ドライブ名とコロンのみ
        if (!_waccess(sTemp.c_str(), 0)){
            break;
        }

        if (IsSeparateChar(sTemp[sTemp.size() - 1])) {
            sTemp = sTemp.substr(0, sTemp.size() - 1);
        }

        listPath.push_front(sTemp); //後で逆順で処理するので
        std::wstring sParentDir, sThisDir;
        SplitFileName(sTemp, sParentDir, sThisDir);
        if (wcscmp(sTemp.c_str(), sParentDir.c_str()) ==0) {
            _ASSERTE(false);//無限ループ防止
            break;
        }
        sTemp = sParentDir;
    }

    //ディレクトリの作成
    std::list<std::wstring>::const_iterator it;
    for (it = listPath.begin(); it != listPath.end(); ++it) {
        BOOL bReturn = CreateDirectoryW(it->c_str(), NULL);
        if (!bReturn) {
            if (psCannotCreateDir) {
                *psCannotCreateDir = *it;
            }
            return false;
        }
    }

    return true;
}

/** @brief ディレクトリ内のファイルも含めて削除する
 *
 * @param sPath [in] 削除するディレクトリ
 * @return 正常終了 true
 */
bool RemoveDirectories(const std::wstring& sPath)
{
    if (sPath.empty()) {
        _ASSERTE(false);
        return false;
    }
    std::wstring sParentDir = sPath;
    if (!IsSeparateChar(sParentDir[sParentDir.size() - 1])) {
        sParentDir += L"\\";
    }
    std::wstring sFindString = sParentDir + L"*";
    WIN32_FIND_DATA fileData;
    HANDLE fileHandle = FindFirstFile(sFindString.c_str(), &fileData);
    while (fileHandle != INVALID_HANDLE_VALUE) {
        if (_tcscmp(fileData.cFileName, L".") != 0
            && _tcscmp(fileData.cFileName, L"..") != 0) {
            std::wstring sFullPath = sParentDir + fileData.cFileName;
            if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                RemoveDirectories(sFullPath);
            } else {
                _wremove(sFullPath.c_str());
            }
        }
        if (!FindNextFile(fileHandle, &fileData)) {
            break;
        }
    }
    FindClose(fileHandle);
    RemoveDirectory(sPath.c_str());
    return true;
}

/** @brief パスを接続する
 *
 * @param[in] sDir  ディレクトリ
 * @param[in] sPath それに続くパス
 * @return 接続したパスの文字列
 */
std::wstring ConnectPath(const std::wstring& sDir, const std::wstring& sPath)
{
    if (sDir.empty()) {
        return sPath;
    }
    if (sPath.empty()) {
        return sDir;
    }

    std::wstring s;
    s = sDir;
    if (!IsSeparateChar(s[s.size() - 1])) {
        s += L'\\';
    }

    if (IsSeparateChar(sPath[0])) {
        s += sPath.substr(1);
    } else {
        s += sPath;
    }
    return s;
}

/** @brief パスを接続する
 *
 * @param[in] sBaseDir  ディレクトリ
 * @param[in] sDir1 それに続くパス
 * @param[in] sPath それに続くパス
 * @return 接続したパスの文字列
 */
std::wstring ConnectPath(const std::wstring& sBaseDir, const std::wstring& sDir1, const std::wstring& sPath)
{
    return ConnectPath(ConnectPath(sBaseDir, sDir1), sPath);
}

/** @brief パスを接続する
 *
 * @param[in] sBaseDir  ディレクトリ
 * @param[in] sDir1 それに続くパス
 * @param[in] sDir2 それに続くパス
 * @param[in] sPath それに続くパス
 * @return 接続したパスの文字列
 */
std::wstring ConnectPath(const std::wstring& sBaseDir, const std::wstring& sDir1, const std::wstring& sDir2, const std::wstring& sPath)
{
    return ConnectPath(ConnectPath(sBaseDir, sDir1, sDir2), sPath);
}

/** @brief パスを接続する
 *
 * @param[in] sDir  ディレクトリ
 * @param[in] sPath それに続くパス
 * @return 接続したパスの文字列
 */
std::string ConnectPath(const std::string& sDir, const std::string& sPath)
{
    if (sDir.empty()) {
        return sPath;
    }
    if (sPath.empty()) {
        return sDir;
    }

    std::string s;
    s = sDir;
    if (!IsSeparateChar(s[s.size() - 1])) {
        s += '\\';
    }

    if (IsSeparateChar(sPath[0])) {
        s += sPath.substr(1);
    } else {
        s += sPath;
    }
    return s;
}

/** @brief パスを接続する
 *
 * @param[in] sBaseDir  ディレクトリ
 * @param[in] sDir1 それに続くパス
 * @param[in] sPath それに続くパス
 * @return 接続したパスの文字列
 */
std::string ConnectPath(const std::string& sBaseDir, const std::string& sDir1, const std::string& sPath)
{
    return ConnectPath(ConnectPath(sBaseDir, sDir1), sPath);
}

/** @brief パスを接続する
 *
 * @param[in] sBaseDir  ディレクトリ
 * @param[in] sDir1 それに続くパス
 * @param[in] sDir2 それに続くパス
 * @param[in] sPath それに続くパス
 * @return 接続したパスの文字列
 */
std::string ConnectPath(const std::string& sBaseDir, const std::string& sDir1, const std::string& sDir2, const std::string& sPath)
{
    return ConnectPath(ConnectPath(sBaseDir, sDir1, sDir2), sPath);
}

///
/// ファイル名からネットワーク上のファイルであるかを判断
///
/// @param  sFullPath [i] ファイル名
///
/// @return ネットワーク上のファイルの場合true
///
bool IsNetworkFile(const std::string& sFullPath)
{
    const char szNetWrokPathHeader[] = "\\\\";
    if (strncmp(sFullPath.c_str(), szNetWrokPathHeader, strlen(szNetWrokPathHeader)) == 0) {
        return true;
    }

    std::string sDrive = sFullPath.substr(0, 3);
    if (!isalpha(sDrive[0]) || sDrive[1] != L':' || sDrive[2] != L'\\') {
        _ASSERTE(false);
        return false;
    }

    // ドライブの種類
    return GetDriveTypeA(sDrive.c_str()) == DRIVE_REMOTE;
}

///
/// ファイル名からネットワーク上のファイルであるかを判断
///
/// @param  sFullPath [i] ファイル名
///
/// @return ネットワーク上のファイルの場合true
///
bool IsNetworkFile(const std::wstring& sFullPath)
{
    const wchar_t szNetWrokPathHeader[] = L"\\\\";
    if (wcsncmp(sFullPath.c_str(), szNetWrokPathHeader, wcslen(szNetWrokPathHeader)) == 0) {
        return true;
    }

    std::wstring sDrive = sFullPath.substr(0, 3);
    if (!iswalpha(sDrive[0]) || sDrive[1] != L':' || sDrive[2] != L'\\') {
        _ASSERTE(false);
        return false;
    }

    // ドライブの種類
    return GetDriveTypeW(sDrive.c_str()) == DRIVE_REMOTE;
}

bool IsHddFile(const std::wstring& sFullPath)
{
    if (sFullPath.empty()) {
        return false;
    }
    const wchar_t szNetWrokPathHeader[] = L"\\\\";
    if (wcsncmp(sFullPath.c_str(), szNetWrokPathHeader, wcslen(szNetWrokPathHeader)) == 0) {
        return false;
    }

    std::wstring sDrive = sFullPath.substr(0, 3);
    if (!iswalpha(sDrive[0]) || sDrive[1] != L':' || sDrive[2] != L'\\') {
        _ASSERTE(false);
        return false;
    }

    // ドライブの種類
    return GetDriveType(sDrive.c_str()) == DRIVE_FIXED;
}

///
/// フルパスであるかを判断
///
/// @param  sPath  [i] ファイル名
///
/// @return フルパスの場合true,相対パスの場合falseを返す
///
/// @note   文字列の先頭がアルファベットと':'の場合
///         もしくは\\の場合をフルパスとみなす
///
bool IsFullPath(const std::string& sPath)
{
    const char szNetWrokPathHeader[] = "\\\\";
    if (strncmp(sPath.c_str(), szNetWrokPathHeader, strlen(szNetWrokPathHeader)) == 0) {
        return true;
    }
    if (sPath.length() >= 2 && isalpha(sPath[0]) && sPath[1] == ':') {
        return true;
    }
    return false;
}

///
/// フルパスであるかを判断
///
/// @param  sPath  [i] ファイル名
///
/// @return フルパスの場合true,相対パスの場合falseを返す
///
/// @note   文字列の先頭がアルファベットと':'の場合
///         もしくは\\の場合をフルパスとみなす
///
bool IsFullPath(const std::wstring& sPath)
{
    const wchar_t szNetWrokPathHeader[] = L"\\\\";
    if (wcsncmp(sPath.c_str(), szNetWrokPathHeader, wcslen(szNetWrokPathHeader)) == 0) {
        return true;
    }
    if (sPath.length() >= 2 && iswalpha(sPath[0]) && sPath[1] == L':') {
        return true;
    }
    return false;
}

/** @brief 文字列に含まれるアルファベットの大文字を小文字にする
 *
 * @param[in] s 文字列
 * @return 大文字を小文字にした文字列
 */
std::wstring ToLower(const std::wstring& s)
{
    //std::wstring sConvert = s;
    //transform(sConvert.begin(), sConvert.end(), sConvert.begin(), towlower);
    //return sConvert;
    std::wstring sReturn;
    size_t iSize = s.size();
    for (size_t i = 0; i < iSize; ++i) {
        if (iswupper(s[i])) {
            sReturn += towlower(s[i]);
        } else {
            sReturn += s[i];
        }
    }
    return sReturn;
}

/** @brief 文字列を小文字に変換
 *
 * @param[in] s 文字列
 * @return 大文字を小文字に変換した文字列
 */
std::string ToLower(const std::string& s)
{
    std::string sLower;
    unsigned int i = 0;
    while (i < s.size()) {
        if (IsTwoBytesChar(s[i])) {
            sLower += s[i];
            ++i;
            sLower += s[i];
            ++i;
        } else if (isupper(s[i])) {
            sLower += tolower(s[i]);
            ++i;
        } else {
            sLower += s[i];
            ++i;
        }
    }
    return sLower;
}

/** @brief ファイル名の拡張子を変更し、別のファイル名を作成する
 *
 * @param[in] sFileName     元のファイル名
 * @param[in] sNewExt       新しい拡張子
 * @return 新しいファイル名
 */
std::wstring ChangeFileExtension(const std::wstring& sFileName, const std::wstring& sNewExt)
{
    std::wstring sBody, sExt;
    SplitFileExtension(sFileName, sBody, sExt);
    return sBody + L"." + sNewExt;
}

/** @brief ファイル名に拡張子を追加
 *
 * @param[in] sFile         ファイル名
 * @param[in] sExt          拡張子
 * @return 新しいファイル名
 */
std::wstring AddExtension(const std::wstring& sFile, const std::wstring& sExt)
{
    return sFile + L"." + sExt;
}

/** @brief ファイルの拡張子が一致しているかを調べる
 *
 * @param[in] sFileName     チェックするファイル名
 * @param[in] sExt          拡張子(これと一致するかを調べる)
 * @return 一致している場合、true
 */
bool IsFileExtension(const std::wstring& sFileName, const std::wstring& sExt)
{
    std::wstring sSplitBody, sSplitExt;
    SplitFileExtension(sFileName, sSplitBody, sSplitExt);
    sSplitExt = ToLower(sSplitExt);
    return sSplitExt == sExt;
}

struct equal_char_ignorecase
{
    bool operator()(wchar_t c1, wchar_t c2) const
    {
        std::locale loc;
        return std::tolower(c1, loc) == std::tolower(c2, loc);
    }
};

//カレントからの相対パスなどは考慮しない
bool IsSamePath(const std::wstring& path1, const std::wstring& path2)
{
    return path1.size() == path2.size()
        && std::equal(path1.begin(), path1.end(), path2.begin(), equal_char_ignorecase());
}

std::wstring DoubleToWString(double d)
{
    wchar_t sz[20];
#if _MSC_VER >= 1400
    swprintf_s(sz, 20, L"%f", d);
#else
    swprintf(sz, L"%f", d);
#endif
    std::wstring s = sz;
    if (s.find(L".") == std::wstring::npos) {
        return s;
    }
    while (!s.empty() && s[s.size() - 1] == L'0') {
        s = s.substr(0, s.size() - 1);
    }
    if (!s.empty() && s[s.size() - 1] == L'.') {
        s = s.substr(0, s.size() - 1);
    }
    return s;
}

std::wstring ColorToWString(COLORREF color)
{
    wchar_t szColor[20];
#if _MSC_VER >= 1400
    swprintf_s(szColor, 20, L"#%02x%02x%02x", GetRValue(color), GetGValue(color), GetBValue(color));
#else
    swprintf(szColor, L"#%02x%02x%02x", GetRValue(color), GetGValue(color), GetBValue(color));
#endif
    return szColor;
}

bool IsZeroString(LPCTSTR sz)
{
    bool bFoundPiriod = false;
    LPCTSTR pc = sz;
    while (*pc) {
        if (*pc == L'0') {
            //0はOKとする
        } else if (*pc == L'.') {
            if (bFoundPiriod) {
                return false;
            }
            bFoundPiriod = true;
        } else {
            return false;
        }
        ++pc;
    }
    return true;
}

std::wstring GetModuleDir()
{
    wchar_t szModuleFullPath[MAX_PATH];
    ::GetModuleFileNameW(NULL, szModuleFullPath, MAX_PATH);
    std::wstring sPath, sFile;
    SplitFileName(szModuleFullPath, sPath, sFile);
    _ASSERTE(!sPath.empty());
    return sPath;
}

std::wstring EscapePercentage(const std::wstring& s)
{
    std::wstring sReplace = s;

    size_t iFindStart = 0;
    while (true) {
        size_t iFind = sReplace.find(L'%', iFindStart);
        if (iFind == std::wstring::npos) {
            break;
        }
        sReplace.replace(iFind, 1, L"%%");
        iFindStart = iFind + 2;
    }
    return sReplace;
}




//以下NewIPASのプロジェクトValueParserよりコピー
static const wchar_t s_cPeriod = '.';
static const std::wstring s_sSign = L"+-";
static const std::wstring s_sExponentChar = L"Ee";

///
/// 数値を示す文字であるか
///
/// @param  c [i] 検査対象の文字
///
/// @return 数値の文字の場合true
///
inline bool IsDigit(wchar_t c)
{
    return c >= L'0' && c <= L'9';
//    return (s_sDigit.find_first_of(c) != wstring::npos);
}

///
/// '+','-'の記号であるか
///
/// @param  c [i] 検査対象の文字
///
/// @return '+','-'の記号の場合true
///
inline bool IsSign(wchar_t c)
{
    return (s_sSign.find_first_of(c) != std::wstring::npos);
}

///
/// 数字の長さを返す
///
/// @param  pc [i] 入力文字列
///
/// @return 数字の長さ
///
inline size_t LengthDigitSequence(const wchar_t* pc)
{
    const wchar_t* p = pc;
    while (*p != L'\0' && IsDigit(*p)) {
        ++p;
    }

    return p - pc;
}

///
/// 小数の長さを返す
///
/// @param  pc [i] 入力文字列
///
/// @return 小数の長さ
///
inline size_t LengthFractionalConstant(const wchar_t* pc)
{
    size_t iLength = 0;
    iLength += LengthDigitSequence(pc);
    if (pc[iLength] == L'\0') {
        return 0;
    }
    if (pc[iLength] != s_cPeriod) {
        return 0;
    }
    iLength++;
    if (pc[iLength] != L'\0') {
        iLength += LengthDigitSequence( pc + iLength );
    }
    return iLength;
}

///
/// @brief   指数表示の数値の長さを返す
///
/// @param[in]  pc  入力文字列
///
/// @return 指数表示の数値の長さ
///
inline long LengthExponent(const wchar_t* pc)
{
    if (pc == NULL || pc[0] == L'\0') {
        return 0;
    }
    if (s_sExponentChar.find_first_of(pc[0]) == std::wstring::npos) {
        return 0;
    }
    int iLength = 1;    //Eまたはe長さ
    if ( IsSign(pc[iLength]) ) {
        iLength += 1;
    }

    int iDigitLength = LengthDigitSequence( pc + iLength );
    if (iDigitLength == 0) {
        return 0;
    }
    return iLength + iDigitLength;
}

///
/// 指数表示もしくは小数の長さを返す
///
/// @param  pc [i] 入力文字列
///
/// @return 指数表示もしくは小数の長さ
///
inline size_t LengthFloatingPointConstant(const wchar_t* pc)
{
    size_t iLength = LengthFractionalConstant(pc);
    if (iLength != 0) {
        if (pc[iLength] != L'\0') {
            iLength += LengthExponent( pc + iLength );
        }
    } else {
        iLength = LengthDigitSequence(pc);
        if (iLength == 0) {
            return 0;
        }
        if (pc[iLength] == L'\0') {
            return 0;
        }
        size_t iExponentLength = LengthExponent( pc + iLength );
        if (iExponentLength == 0) {
            return 0;
        }
        iLength += iExponentLength;
    }
    return iLength;
}

///
/// 整数の長さを返す
///
/// @param  pc [i] 入力文字列
///
/// @return 整数の長さ
///
inline size_t LengthIntegerConstant(const wchar_t* pc)
{
    return LengthDigitSequence(pc);
}

///
/// 数値の長さを返す
///
/// @param  pc [i] 入力文字列
///
/// @return 数値の長さ
///
size_t LengthNumber(const wchar_t* pc)
{
    if (pc == NULL || pc[0] == L'\0') {
        return 0;
    }
    size_t iLength = 0;
    if ( IsSign(pc[0]) ) {
        iLength += 1;
        if (pc[iLength] == L'\0') {
            return 0;
        }
    }
    size_t iFloatingPointConstantLength = LengthFloatingPointConstant( pc + iLength );
    if (iFloatingPointConstantLength != 0) {
        iLength += iFloatingPointConstantLength;
    } else {
        size_t iIntegerLength = LengthIntegerConstant( pc + iLength );
        if (iIntegerLength == 0) {
            return 0;
        }
        iLength += iIntegerLength;
    }
    return iLength;
}

///
/// @brief   数値の読み込み
///
/// @param[out] d 読み込んだ数値
/// @param[in] s 入力文字列
///
/// @return 読み込めた場合 true
///
/// @note   読み込んだ文字列と、次の文字列までの','と空白は削除される
///
bool ReadNumber(double& d, std::wstring& s)
{
    int iLength = LengthNumber(s.c_str());
    if (iLength == 0) {
        return false;
    }
    d = _wtof(s.c_str());
    s.erase(0, iLength);
    return true;
}
