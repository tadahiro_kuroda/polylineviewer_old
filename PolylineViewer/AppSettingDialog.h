#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#include "ColorButton.h"
#include "afxeditbrowsectrl.h"

// AppSettingDialog ダイアログ

class AppSettingDialog : public CDialogEx
{
	DECLARE_DYNAMIC(AppSettingDialog)

public:
	AppSettingDialog(CWnd* pParent = NULL);   // 標準コンストラクター
	virtual ~AppSettingDialog();
	BOOL OnInitDialog();

// ダイアログ データ
	enum { IDD = IDD_APP_SETTING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	DECLARE_MESSAGE_MAP()

private:
	ColorButton m_ColorButtonNewPolyline;
	ColorButton m_ColorButtonExistPolyline;
    int m_iNewPolylineWidth;
    int m_iExistPolylineWidth;
	BOOL m_bUseRandomColor;
	CMFCEditBrowseCtrl m_ctlColorFilePath;

protected:
    void EnableNewRoadColorCtl();

public:
    virtual void OnOK();
	afx_msg void OnBnClickedColorButtonNewPolyline();
	afx_msg void OnBnClickedColorButtonExistPolyline();
    afx_msg void OnBnClickedCheckRandomColor();
};
