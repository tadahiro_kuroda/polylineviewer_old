// LineSettingDialog.cpp : 実装ファイル
//

#include "stdafx.h"
#include "PolylineViewer.h"
#include "AppSettingDialog.h"
//#include "afxdialogex.h"

#include "AppSetting.h"
#include "ColorTableFile.h"
#include "PolylineViewerView.h"
#include "RegistryUtil.h"

// AppSettingDialog ダイアログ

IMPLEMENT_DYNAMIC(AppSettingDialog, CDialogEx)

AppSettingDialog::AppSettingDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(AppSettingDialog::IDD, pParent)
	, m_bUseRandomColor(FALSE)
{

}

AppSettingDialog::~AppSettingDialog()
{
}

void AppSettingDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COLOR_BUTTON_NEW_POLYLINE, m_ColorButtonNewPolyline);
	DDX_Control(pDX, IDC_COLOR_BUTTON_EXIST_POLYLINE, m_ColorButtonExistPolyline);
	DDX_Text(pDX, IDC_EDIT_WIDTH_NEW_POLYLINE, m_iNewPolylineWidth);
	DDV_MinMaxInt(pDX, m_iNewPolylineWidth, 1, 100);
	DDX_Text(pDX, IDC_EDIT_WIDTH_EXIST_POLYLINE, m_iExistPolylineWidth);
	DDV_MinMaxInt(pDX, m_iExistPolylineWidth, 1, 100);

	DDX_Check(pDX, IDC_CHECK_RANDOM_COLOR, m_bUseRandomColor);
	DDX_Control(pDX, IDC_MFCEDITBROWSE_RANDOM_COLOR, m_ctlColorFilePath);
}


BEGIN_MESSAGE_MAP(AppSettingDialog, CDialogEx)
	ON_BN_CLICKED(IDC_COLOR_BUTTON_NEW_POLYLINE, &AppSettingDialog::OnBnClickedColorButtonNewPolyline)
	ON_BN_CLICKED(IDC_COLOR_BUTTON_EXIST_POLYLINE, &AppSettingDialog::OnBnClickedColorButtonExistPolyline)
    ON_BN_CLICKED(IDC_CHECK_RANDOM_COLOR, &AppSettingDialog::OnBnClickedCheckRandomColor)
END_MESSAGE_MAP()


// AppSettingDialog メッセージ ハンドラー

BOOL AppSettingDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    std::shared_ptr<AppSetting> pSetting = theApp.GetAppSetting();
    m_iNewPolylineWidth = pSetting->iNewPolylineWidth;
    m_iExistPolylineWidth = pSetting->iExistPolylineWidth;
    m_ColorButtonNewPolyline.SetColor(pSetting->colorNewPolyline);
    m_ColorButtonExistPolyline.SetColor(pSetting->colorExistPolyline);
    m_bUseRandomColor = pSetting->bUseRandomColor;

    m_ctlColorFilePath.SetWindowText((LPCTSTR)pSetting->sRandomColorFilePath);
    m_ctlColorFilePath.EnableFileBrowseButton(
			_T("txt"),
			_T("Text files(*.txt)|*.txt|all files(*.*)|*.*||")
			//_T("tsv files(*.tsv)|*.tsv|csv files(*.csv)|*.csv|txt files(*.txt)|*.txt|tsv csv txt(*.tsv;*.csv;*.txt)|*.tsv;*.csv;*.txt|all files(*.*)|*.*||")
		);

    UpdateData(FALSE);
    EnableNewRoadColorCtl();
    return TRUE;
}

void AppSettingDialog::OnBnClickedColorButtonNewPolyline()
{
    CColorDialog dlg(m_ColorButtonNewPolyline.GetColor(), 0, this);
    if (IDOK != dlg.DoModal()) {
        return;
    }
    m_ColorButtonNewPolyline.SetColor(dlg.GetColor());
}


void AppSettingDialog::OnBnClickedColorButtonExistPolyline()
{
    CColorDialog dlg(m_ColorButtonExistPolyline.GetColor(), 0, this);
    if (IDOK != dlg.DoModal()) {
        return;
    }
    m_ColorButtonExistPolyline.SetColor(dlg.GetColor());
}

void AppSettingDialog::OnOK()
{
    UpdateData(TRUE);

    std::shared_ptr<AppSetting> pSetting = theApp.GetAppSetting();
    pSetting->iNewPolylineWidth = m_iNewPolylineWidth;
    pSetting->bUseRandomColor = m_bUseRandomColor != FALSE;

    if (m_bUseRandomColor) {
        m_ctlColorFilePath.GetWindowTextW(pSetting->sRandomColorFilePath);
        if (!theApp.GetColorTable()->Load(pSetting->sRandomColorFilePath)) {
            //ファイルを読み込めないようなら正しいファイルのパスが設定されていない、不正なファイルの可能性があるなど
            ::AfxMessageBox(IDS_ERROR_RANDOM_COLOR_FILE);
            return;
        }
    } else {
        pSetting->colorNewPolyline = m_ColorButtonNewPolyline.GetColor();
    }
    pSetting->iExistPolylineWidth = m_iExistPolylineWidth;
    pSetting->colorExistPolyline = m_ColorButtonExistPolyline.GetColor();

    CDialogEx::OnOK();
}


void AppSettingDialog::OnBnClickedCheckRandomColor()
{
    UpdateData(TRUE);
    EnableNewRoadColorCtl();
}

void AppSettingDialog::EnableNewRoadColorCtl()
{
    GetDlgItem(IDC_MFCEDITBROWSE_RANDOM_COLOR)->EnableWindow(m_bUseRandomColor);
    GetDlgItem(IDC_COLOR_BUTTON_NEW_POLYLINE)->EnableWindow(!m_bUseRandomColor);
}
