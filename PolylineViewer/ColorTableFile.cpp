#include "stdafx.h"
#include "ColorTableFile.h"

#include <fstream>
#include <sstream>
#include <algorithm>

namespace {
    const char s_cStartColor = '#';
    const std::string s_sXDigit = "0123456789abcdefABCDEF";

    /** @brief  16進数の数字の長さを返す
     * 
     *  @param[in]  s 入力文字列
     * 
     *  @return 16進数の数字の長さ
     */ 
    inline size_t LengthXDigitSequence(const std::string& s)
    {
        size_t iTest = s.find_first_not_of(s_sXDigit);
        if (iTest != std::string::npos) {
            return iTest;
        } else {
            return s.size();
        }
    }

    /** @brief  色の文字列の長さを返す
     * 
     *  @param[in]  s 入力文字列
     * 
     *  @return 色の文字列の長さ
     */ 
    size_t LengthColorString(const std::string& s)
    {
        size_t iStringLength = s.size();
        if (iStringLength < 4) {
            return 0;
        }
        if (s[0] != s_cStartColor) {
            return 0;
        }
        size_t iLength = LengthXDigitSequence(s.substr(1));
        if (iLength != 3 && iLength != 6) {
            return 0;
        }
        return  1 + iLength;
    }

    /** @brief  wchar_t型から16進数の数字を返す. 0〜9、a〜f以外の文字ががくることは想定していない
     *  @param[in]  cHex   変換する文字
     *  @return 16進数での値 */
    int HexadecimalCharToDecimalColorNumber(char cHex)
    {
        if ('a' <= cHex && cHex <= 'f') {
            return cHex - 'a' + 10;
        }
        if ('A' <= cHex && cHex <= 'F') {
            return cHex - 'A' + 10;
        }
        if ('0' <= cHex && cHex <= '9') {
            return cHex - '0';
        }
        _ASSERTE(0);
        return 0;
    }

    //色の読み込み
    bool LoadColor(const std::string& s, COLORREF& color)
    {
        size_t iLength = LengthColorString(s.c_str());
        if (iLength == 7) {    // "#00ff88" ってな形なので
            color = RGB(HexadecimalCharToDecimalColorNumber(s[1]) * 16 + HexadecimalCharToDecimalColorNumber(s[2]),
                        HexadecimalCharToDecimalColorNumber(s[3]) * 16 + HexadecimalCharToDecimalColorNumber(s[4]),
                        HexadecimalCharToDecimalColorNumber(s[5]) * 16 + HexadecimalCharToDecimalColorNumber(s[6]));
            return true;
        } else if (iLength == 4) {
            color = RGB(HexadecimalCharToDecimalColorNumber(s[1]) * 17,
                        HexadecimalCharToDecimalColorNumber(s[2]) * 17,
                        HexadecimalCharToDecimalColorNumber(s[3]) * 17);
            return true;
        }
        return false;

    }
}

ColorTableFile::ColorTableFile(void)
{
}


ColorTableFile::~ColorTableFile(void)
{
}

bool ColorTableFile::Load(LPCTSTR szPath)
{
    std::ifstream ifs(szPath);
    if (!ifs.is_open()) {
		return false;
    }
    std::string line;
    while (!ifs.eof() && std::getline(ifs, line))
    {
        if (line.empty()) {
            continue;
        }
        COLORREF color;
        if (!LoadColor(line, color)) {
            continue;
        }

        m_arrayColor.push_back(color);
    }

	return true;
}

COLORREF ColorTableFile::GetColor(size_t index)
{
    if (m_arrayColor.empty()) {
        _ASSERTE(false);
        return RGB(0,0,0);
    }
    return m_arrayColor[index % m_arrayColor.size()];
}
