#pragma once
#include "afxdialogex.h"
#include "resource.h"
#include "afxwin.h"

// アプリケーションのバージョン情報に使われる CAboutDlg ダイアログ
class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// ダイアログ データ
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

// 実装
protected:
	DECLARE_MESSAGE_MAP()
    virtual BOOL OnInitDialog();

private:
    CStatic m_ctlStaticVersion;
    CStatic m_ctlStaticDate;
};