
// PolylineViewerDoc.h : CPolylineViewerDoc クラスのインターフェイス
//


#include "PolylineFile.h"

#pragma once


class CPolylineViewerDoc : public CDocument
{
protected: // シリアル化からのみ作成します。
	CPolylineViewerDoc();
	DECLARE_DYNCREATE(CPolylineViewerDoc)

// 属性
public:

// 操作
public:

// オーバーライド
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// 実装
public:
	virtual ~CPolylineViewerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	bool LoadNewRoadPolyline(const std::wstring& path);
	static bool LoadExistRoadPolyline(const std::wstring& path);
	//virtual void setNewPolylineFormat();
	//virtual void setExistPolylineFormat();

protected:

// 生成された、メッセージ割り当て関数
protected:
	DECLARE_MESSAGE_MAP()

public:
	std::vector<pv::Polyline> m_vNewPolyline;
	static std::vector<pv::Polyline> s_vExistPolyline;

	bool m_bOnlyExistFlag;	///< 既存道路のみ描画しているViewかどうかのフラグ


#ifdef SHARED_HANDLERS
	// 検索ハンドラーの検索コンテンツを設定するヘルパー関数
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
    virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
    virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
    virtual void OnCloseDocument();
};
