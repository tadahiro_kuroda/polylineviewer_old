#pragma once

struct AppSetting
{
    AppSetting(void);
    ~AppSetting(void);

    void Save();
	void Load();

	int iNewPolylineWidth;	        ///< 新規ポリラインの太さ
	int iExistPolylineWidth;	    ///< 既存ポリラインの太さ
	COLORREF colorNewPolyline;	    ///< 新規ポリラインの色
	COLORREF colorExistPolyline;    ///< 既存ポリラインの色
	bool bUseRandomColor;           /// < 線ごとに色を変えるかどうかのフラグ
	CString sRandomColorFilePath;   /// < ポリラインの色情報ファイルのパス
};

