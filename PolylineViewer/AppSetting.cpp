#include "stdafx.h"
#include "AppSetting.h"

#include "RegistryUtil.h"

//レジストリ設定項目
#define SECTION L"CPolylineViewerView"

#define NEW_POLYLINE_WIDTH      L"NewPolylineWidth"
#define NEW_POLYLINE_COLOR      L"NewPolylineColor"
#define EXIST_POLYLINE_WIDTH    L"ExistPolylineWidth"
#define EXIST_POLYLINE_COLOR    L"ExistPolylineColor"
#define USE_RANDOM_COLOR        L"UseRandomColor"
#define RANDOM_COLOR_FILE_PATH  L"RandomColorFilePath"

AppSetting::AppSetting(void)
: iNewPolylineWidth(3)
, iExistPolylineWidth(3)
, colorNewPolyline(RGB(255,0,0))
, colorExistPolyline(RGB(0,0,255))
, bUseRandomColor(false)
, sRandomColorFilePath(_T(""))
{
}


AppSetting::~AppSetting(void)
{
}

void AppSetting::Save()
{
	RegistryUtil::Save(SECTION, NEW_POLYLINE_WIDTH, iNewPolylineWidth);
	RegistryUtil::Save(SECTION, NEW_POLYLINE_COLOR, colorNewPolyline);
	RegistryUtil::Save(SECTION, EXIST_POLYLINE_WIDTH, iExistPolylineWidth);
	RegistryUtil::Save(SECTION, EXIST_POLYLINE_COLOR, colorExistPolyline);
	RegistryUtil::Save(SECTION, USE_RANDOM_COLOR, bUseRandomColor);
	RegistryUtil::Save(SECTION, RANDOM_COLOR_FILE_PATH, sRandomColorFilePath);
}

void AppSetting::Load()
{
	RegistryUtil::Load(SECTION, NEW_POLYLINE_WIDTH, iNewPolylineWidth);
	RegistryUtil::Load(SECTION, NEW_POLYLINE_COLOR, colorNewPolyline);
	RegistryUtil::Load(SECTION, EXIST_POLYLINE_WIDTH, iExistPolylineWidth);
	RegistryUtil::Load(SECTION, EXIST_POLYLINE_COLOR, colorExistPolyline);
	RegistryUtil::Load(SECTION, USE_RANDOM_COLOR, bUseRandomColor);
	RegistryUtil::Load(SECTION, RANDOM_COLOR_FILE_PATH, sRandomColorFilePath);
}
