#include "stdafx.h"

#include "PolylineFile.h"
#include <fstream>
#include <sstream>
#include <algorithm>

//Linux版でTupleが正しく動かないので、その派生のPoint3.hなどが使用できない
//このため、Matrixの必要部分を自分で定義
#define USE_ORIGINAL_MATRIX

#ifdef USE_ORIGINAL_MATRIX
#include "Point2.h"
#include "Vector2.h"
#else
#include "Point3.h"
#include "Vector3.h"
#include "Matrix3.h"
#endif


#ifdef USE_ORIGINAL_MATRIX
typedef VM_VECMATH_NS::Point2d Point2;
typedef VM_VECMATH_NS::Vector2d Vector;
namespace {
class Matrix
{
public:
    void rotZ(double angle)
    {
	    double c = std::cos(angle);
	    double s = std::sin(angle);
	    m00 = c;   m01 = -s;  m02 = 0.0;
	    m10 = s;   m11 = c;   m12 = 0.0;
	    m20 = 0.0; m21 = 0.0; m22 = 1.0;
    }
    
    Vector operator *(const Vector& vec)
    {
        return Vector(
            m00 * vec.x + m01 * vec.y + m02,
            m10 * vec.x + m11 * vec.y + m12);
    }
private:
    double m00, m01, m02, m10, m11, m12, m20, m21, m22;
};
};
#else
typedef VM_VECMATH_NS::Point3d Point;
typedef VM_VECMATH_NS::Vector3d Vector;
typedef VM_VECMATH_NS::Matrix3<double> Matrix;
#endif

namespace pv{

void Rect::Union(const Rect& other)
{
    if (other.m_empty) {
        return;
    }
    if (m_empty) {
        *this = other;
        return;
    }
    Union(other.m_min);
    Union(other.m_max);
}

void Rect::Union(const Point& pt)
{
    if (m_empty) {
        m_min = pt;
        m_max = pt;
        m_empty = false;
        return;
    }
    m_min.m_x = std::min(m_min.m_x, pt.m_x);
    m_min.m_y = std::min(m_min.m_y, pt.m_y);
    m_max.m_x = std::max(m_max.m_x, pt.m_x);
    m_max.m_y = std::max(m_max.m_y, pt.m_y);
}

std::istream& operator>>(std::istream& is, pv::Polyline& polyline)
{
    size_t n = -1;
    is >> polyline.m_name >> n;
    if (n == -1)
    {
        THROW("invalid polyline file");
    }

    polyline.m_points.resize(n);
    std::for_each(polyline.m_points.begin(), polyline.m_points.end(),
		[=, &is](pv::Point point){
			is >> point.m_x >> point.m_y;
		}
	);
    if (is.fail())
    {
        THROW("invalid polyline file");
    }
    return is;
}

std::ostream& operator<<(std::ostream& os, const pv::Polyline& polyline)
{
    os << polyline.m_name << '\t' << polyline.m_points.size();
    std::for_each(polyline.m_points.begin(), polyline.m_points.end(),
		[=, &os](pv::Point point){
			os << '\t' << point.m_x << '\t' << point.m_y;
		}
	);
	/*
	for each(const auto& point in polyline.m_points)
    {
        os << '\t' << point.m_x << '\t' << point.m_y;
    }
	*/
    return os;
}


// 文字列strをdelimで分割
static std::vector<std::string> split(std::string& str, char delim)
{
	std::istringstream iss(str); 
	std::string temp;
	std::vector<std::string> res;

	while(std::getline(iss, temp, delim)){
		res.push_back(temp);
	}

	return res;
}

/** @brief ポリラインファイルをロードする. */
bool pv::Polyline::Load(const std::wstring& path, std::vector<pv::Polyline>& polylines)
{
    std::ifstream ifs(path);
    if (!ifs.is_open())
    {
		return false;
    }
    std::string line;
    while (!ifs.eof() && std::getline(ifs, line))
    {
        if (line.empty())
        {
            continue;
        }

		pv::Polyline tempPolyline;

		std::vector<std::string> splitStrings = split(line, '\t');
		if(splitStrings.size() < 4) return false;	// 各行は最低でも\tで4つの要素に分割される

		
		int point_num = 0;
		int cnt = 0;
		float tempPointX = 0;
		for(std::vector<std::string>::iterator it = splitStrings.begin(); it != splitStrings.end(); it++)
		{
			if(cnt == 0) tempPolyline.m_name = *it; 
			else if (cnt == 1)
			{
				try{
					point_num = std::stoi(*it);
				}
				catch(std::invalid_argument e){
					return false;
				}
				catch(std::out_of_range e){
					return false;
				}
			}
			else if (cnt % 2 == 0)
			{
				try{
					tempPointX = std::stof(*it);	
				}
				catch(std::invalid_argument e){
					return false;
				}
				catch(std::out_of_range e){
					return false;
				}
			}
			else{
				pv::Point tempPoint;

				tempPoint.m_x = tempPointX;
				try{
					tempPoint.m_y = std::stof(*it);
				}
				catch(std::invalid_argument e){
					return false;
				}
				catch(std::out_of_range e){
					return false;
				}
				
				tempPolyline.m_points.push_back(tempPoint);
			}
			cnt += 1;
		}

		if(point_num != (cnt - 2) / 2) return false;

		polylines.push_back(tempPolyline);

		/*
        polylines.push_back(Polyline());
#if defined(__GNUC__) && (__GNUC__ <= 4) && (__GNUC_MINOR__ <= 4)
        std::istringstream iss(line);
        std::istream& is = iss;
        is >> polylines.back();
#else
        std::istringstream(line) >> polylines.back();
#endif
		*/
    }

	//if(polylines.size() == 0) return false;
	return true;
}



/** @brief ポリラインファイルを保存する. */
void pv::Polyline::Save(const std::wstring& path, const std::vector<pv::Polyline>& polylines)
{
    std::ofstream ofs(path.c_str());
    ofs.exceptions(std::ios::failbit | std::ios::badbit);
    ofs.precision(12);
	std::for_each(polylines.begin(), polylines.end(),
		[=, &ofs](pv::Polyline polyline){
			ofs << polyline << std::endl;
		}
	);
	/*
    for each(const auto& polyline in polylines)
    {
        ofs << polyline << std::endl;
    }
	*/
}

/** @brief バウンディングボックスを取得する. */
Rect pv::Polyline::BoundingBox() const
{
    if (m_points.empty())
    {
        THROW("polyline is empty");
    }

    Rect rect;
    for (size_t i = 0; i < m_points.size(); ++i)
    {
        rect.Union(m_points[i]);
    }
    return rect;
}

namespace {
size_t GetNonZeroIndex(const std::vector<Point2>& points, size_t index)
{
    double TOL_LENGTH = 0.001;
    for (size_t i = index + 1; i < points.size(); ++i) {
        Vector vec = (points[index] - points[i]);
        if (vec.length () > TOL_LENGTH) {
            return i;
        }
    }
    if (index != 0) {
        size_t i = index - 1;
        while (true) {
            Vector vec = (points[index] - points[i]);
            if (vec.length () > TOL_LENGTH) {
                return i;
            }
            if (i == 0) {
                break;
            }
            --i;
        }
    }
    THROW("index is not found");
}
};

void pv::Polyline::MoveRight(double move)
{

    MoveRight(move, m_points);

}

void pv::Polyline::MoveRight(double move, std::vector<pv::Point>& polyline)
{

    Matrix rotate;
    rotate.rotZ(-atan(1) * 2.0);

    std::vector<Point2> points(polyline.size());
    for (size_t i = 0; i < polyline.size(); ++i) {
        points[i].x = polyline[i].m_x;
        points[i].y = polyline[i].m_y;
#ifndef USE_ORIGINAL_MATRIX
        points[i].z = 0;
#endif
    }

    std::vector<Point2> move_points(points.size());
    for (size_t i = 0; i < points.size(); ++i) {
        size_t index = GetNonZeroIndex(points, i);
        Vector vec;
        if (index > i) {
            vec = rotate * (points[index] - points[i]);
        } else {
            vec = rotate * (points[i] - points[index]);
        }
        vec.normalize();
        move_points[i] = points[i] + vec * move;
    }

    for (size_t i = 0; i < move_points.size(); ++i) {
        polyline[i].m_x = move_points[i].x;
        polyline[i].m_y = move_points[i].y;
    }

}

void GetBoundingBox(const std::vector<pv::Polyline>& polylines, pv::Rect& bound)
{
    if (polylines.empty())
    {
        _ASSERTE(false);
        return;
    }

    for (size_t i = 0; i < polylines.size(); ++i)
    {
        bound.Union(polylines[i].BoundingBox());
    }
}
}