
// PolylineViewer.cpp : アプリケーションのクラス動作を定義します。
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "PolylineViewer.h"
#include "MainFrm.h"

#include "AppSetting.h"
#include "AboutDlg.h"
#include "ChildFrm.h"
#include "ColorTableFile.h"
#include "PolylineViewerDoc.h"
#include "PolylineViewerView.h"
#include "RegistryUtil.h"
#include "Utils.h"
#include "AppSettingDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

namespace {
// 要確認
/** GdiplusStartup, GdiplusShutdown の呼び出しを自動化するクラス */
class GDIPlusActivate
{
public:
    GDIPlusActivate() {
        GdiplusStartup(&gdiToken, &gdiSI, NULL);
    }
    virtual ~GDIPlusActivate() {
        Gdiplus::GdiplusShutdown(gdiToken);
    }
protected:
    Gdiplus::GdiplusStartupInput gdiSI;
    ULONG_PTR           gdiToken;
};
std::shared_ptr<GDIPlusActivate> g_pGdiplusActivate;

};

//すべてのView、Documentに対して処理を行うためのコールバック関数
namespace {
	void FitAndInvalidate(CView* pView, void* pParam)
	{
		CPolylineViewerView* pThisView = dynamic_cast<CPolylineViewerView*>(pView);
		if (!pThisView) {
			_ASSERTE(false);
			return;
		}
		pThisView->Fit();
		pThisView->Invalidate();
	}

	void InvalidateAllViewFunc(CDocument* pDoc, void* pParam)
	{
		pDoc->UpdateAllViews(NULL);
	}

    void FindOnlyExistRoadDoc(CDocument* pDoc, void* pParam)
    {
        bool* pbFoundExistRoadDoc = static_cast<bool*>(pParam);
        if (!pbFoundExistRoadDoc) {
            _ASSERTE(false);    //呼び出しのパラメータ設定がおかしい
            return;
        }
        if (*pbFoundExistRoadDoc) {
            return; //すでに見つかっている
        }

		CPolylineViewerDoc* pThisDoc = dynamic_cast<CPolylineViewerDoc*>(pDoc);
        if (pThisDoc->m_bOnlyExistFlag) {
            *pbFoundExistRoadDoc = true;
        }
    }

    void GetLoadedDocumentPath(CDocument* pDoc, void* pParam)
    {
        std::set<std::wstring>* pSetPath = (std::set<std::wstring>*)pParam;
        if (!pSetPath) {
            _ASSERTE(false);    //パラメータ設定がおかしい
            return;
        }
        pSetPath->insert(ToLower((LPCTSTR)pDoc->GetPathName()));
    }

	void SaveBmp(CView* pView, void* pParam)
	{
        std::wstring* pDir = static_cast<std::wstring*>(pParam);

		CPolylineViewerView* pThisView = dynamic_cast<CPolylineViewerView*>(pView);
		if (!pThisView) {
			_ASSERTE(false);
			return;
		}

        //ViewからDocを取得
        //std::wstring sFile = タイトル文字列からファイル名作成;
        //std::wstring sPath = ConnectPath(*pDir, sFile);
        //pThisView->BmpOutput(sPath);
	}
}


// CPolylineViewerApp

BEGIN_MESSAGE_MAP(CPolylineViewerApp, CWinAppEx)
	ON_COMMAND(ID_APP_ABOUT, &CPolylineViewerApp::OnAppAbout)
	// 標準のファイル基本ドキュメント コマンド
	ON_COMMAND(ID_FILE_NEW, &CWinAppEx::OnFileNew)
	//ON_COMMAND(ID_FILE_OPEN, &CWinAppEx::OnFileOpen)
	// 標準の印刷セットアップ コマンド
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinAppEx::OnFilePrintSetup)
	ON_COMMAND(ID_NEW_POLYLINE_OPEN, &CPolylineViewerApp::OnNewPolylineOpen)
    ON_UPDATE_COMMAND_UI(ID_NEW_POLYLINE_OPEN, &CPolylineViewerApp::OnUpdateNewPolylineOpen)
	ON_COMMAND(ID_EXIST_POLYLINE_OPEN, &CPolylineViewerApp::OnExistPolylineOpen)
    ON_UPDATE_COMMAND_UI(ID_EXIST_POLYLINE_OPEN, &CPolylineViewerApp::OnUpdateExistPolylineOpen)
    ON_COMMAND(ID_APP_SETTING, &CPolylineViewerApp::OnAppSetting)
    ON_UPDATE_COMMAND_UI(ID_APP_SETTING, &CPolylineViewerApp::OnUpdateAppSetting)
    ON_COMMAND(ID_BMP_OUTPUT_ALL, &CPolylineViewerApp::OnBmpOutputAll)
    ON_UPDATE_COMMAND_UI(ID_BMP_OUTPUT_ALL, &CPolylineViewerApp::OnUpdateBmpOutputAll)
END_MESSAGE_MAP()


// CPolylineViewerApp コンストラクション

CPolylineViewerApp::CPolylineViewerApp()
    : m_pSettting(new AppSetting())
    , m_pColorTable(new ColorTableFile())
{
	m_bHiColorIcons = TRUE;

	// TODO: 下のアプリケーション ID 文字列を一意の ID 文字列で置換します。推奨される
	// 文字列の形式は CompanyName.ProductName.SubProduct.VersionInformation です
	SetAppID(_T("PolylineViewer.AppID.NoVersion"));

	// TODO: この位置に構築用コードを追加してください。
	// ここに InitInstance 中の重要な初期化処理をすべて記述してください。
}

// 唯一の CPolylineViewerApp オブジェクトです。

CPolylineViewerApp theApp;


// CPolylineViewerApp 初期化

BOOL CPolylineViewerApp::InitInstance()
{
	// アプリケーション マニフェストが visual スタイルを有効にするために、
	// ComCtl32.dll Version 6 以降の使用を指定する場合は、
	// Windows XP に InitCommonControlsEx() が必要です。さもなければ、ウィンドウ作成はすべて失敗します。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// アプリケーションで使用するすべてのコモン コントロール クラスを含めるには、
	// これを設定します。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();


	EnableTaskbarInteraction(FALSE);

	// RichEdit コントロールを使用するには AfxInitRichEdit2() が必要です	
	// AfxInitRichEdit2();

	// 標準初期化
	// これらの機能を使わずに最終的な実行可能ファイルの
	// サイズを縮小したい場合は、以下から不要な初期化
	// ルーチンを削除してください。
	// 設定が格納されているレジストリ キーを変更します。
	// TODO: 会社名または組織名などの適切な文字列に
	// この文字列を変更してください。
	SetRegistryKey(_T("アプリケーション ウィザードで生成されたローカル アプリケーション"));
	LoadStdProfileSettings(4);  // 標準の INI ファイルのオプションをロードします (MRU を含む)

	// レジストリベースの初期化
	RegistryUtil::SetBase(std::wstring(L"Software\\Zodiac\\PolylineViewer\\"));

	InitContextMenuManager();

	InitKeyboardManager();

	InitTooltipManager();
	CMFCToolTipInfo ttParams;
	ttParams.m_bVislManagerTheme = TRUE;
	theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL,
		RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

	// アプリケーション用のドキュメント テンプレートを登録します。ドキュメント テンプレート
	//  はドキュメント、フレーム ウィンドウとビューを結合するために機能します。
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_PolylineViewerTYPE,
		RUNTIME_CLASS(CPolylineViewerDoc),
		RUNTIME_CLASS(CChildFrame), // カスタム MDI 子フレーム
		RUNTIME_CLASS(CPolylineViewerView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	// メイン MDI フレーム ウィンドウを作成します。
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
	{
		delete pMainFrame;
		return FALSE;
	}
	m_pMainWnd = pMainFrame;


	// DDE、file open など標準のシェル コマンドのコマンド ラインを解析します。
	CCommandLineInfo cmdInfo;
    //起動時に新規ドキュメントを作成しない
    if (cmdInfo.m_nShellCommand == CCommandLineInfo::FileNew) {
        cmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;
    }
    ParseCommandLine(cmdInfo);

    //GDI+初期化
    g_pGdiplusActivate.reset(new GDIPlusActivate);


	// コマンド ラインで指定されたディスパッチ コマンドです。アプリケーションが
	// /RegServer、/Register、/Unregserver または /Unregister で起動された場合、False を返します。
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
	// メイン ウィンドウが初期化されたので、表示と更新を行います。
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

    CPolylineViewerView::LoadFlag();
    m_pSettting->Load();
    if (m_pSettting->bUseRandomColor && !m_pSettting->sRandomColorFilePath.IsEmpty()) {
        if (!m_pColorTable->Load(m_pSettting->sRandomColorFilePath)) {
            //色テーブルファイルが読み込めないようなら設定を変更
            m_pSettting->bUseRandomColor = false;
        }
    }

	return TRUE;
}

int CPolylineViewerApp::ExitInstance()
{
    CPolylineViewerView::SaveFlag();
    m_pSettting->Save();

    g_pGdiplusActivate.reset();

	return CWinAppEx::ExitInstance();
}

// CPolylineViewerApp メッセージ ハンドラー



// ダイアログを実行するためのアプリケーション コマンド
void CPolylineViewerApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CPolylineViewerApp のカスタマイズされた読み込みメソッドと保存メソッド

void CPolylineViewerApp::PreLoadState()
{
	//BOOL bNameValid;
	//CString strName;
	//bNameValid = strName.LoadString(IDS_EDIT_MENU);
	//ASSERT(bNameValid);
	//GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
}

void CPolylineViewerApp::LoadCustomState()
{
}

void CPolylineViewerApp::SaveCustomState()
{
}

// CPolylineViewerApp メッセージ ハンドラー



void CPolylineViewerApp::OnNewPolylineOpen()
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString sPathName;
	RegistryUtil::Load(L"File", L"LastNewPolylineFile", sPathName);

    std::wstring sDir, sFile;
    if (!sPathName.IsEmpty()) {
        SplitFileName((LPCTSTR)sPathName, sDir, sFile);
    }

	// ファイルパスの指定
	const int MAX_FILE = 200;
	const int BUFFER_SIZE = FILENAME_MAX * MAX_FILE;
	std::vector<TCHAR> buffer;
	buffer.resize(BUFFER_SIZE, 0);

    CFileDialog dlg(TRUE, L"*.txt", sFile.c_str(), 
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_ALLOWMULTISELECT,
		L"Text Files(*.txt)|*.txt|All Files(*.*)|*.*||",
		NULL,	0,	TRUE
	);
	dlg.m_ofn.lpstrFile = &(buffer[0]);
	dlg.m_ofn.nMaxFile = BUFFER_SIZE;
    OPENFILENAME& ofn = dlg.GetOFN();
    ofn.lpstrInitialDir = sDir.c_str();
    CString sTitle(_T("Choose New Road Polyline file"));
    ofn.lpstrTitle = sTitle;

    if (dlg.DoModal() != IDOK ){
        return;
    }

    //すでに読み込み済みの全てのパスを取得
    std::set<std::wstring> setPath;
    DocumentWalk(GetLoadedDocumentPath, &setPath);

	CMainFrame* pMain = static_cast<CMainFrame*>((AfxGetMainWnd()));

    bool bFoundSkipFile = false;
	int iFileCount = 0;
	POSITION pos = dlg.GetStartPosition();
	while(pos) {
		sPathName = dlg.GetNextPathName(pos);

        //すでに読み込み済みのファイルの場合、スキップ
        if (setPath.find(ToLower((LPCTSTR)sPathName)) != setPath.end()) {
            bFoundSkipFile = true;
            continue;
        }
        setPath.insert((LPCTSTR)sPathName); //ファイルダイアログで同じパスは選択できないはずだが念のため

		if (!sPathName.IsEmpty()) {
			SplitFileName((LPCTSTR)sPathName, sDir, sFile);
		}

		CWinApp::OnFileNew();

		// View,Docの取得
		CMDIChildWnd* pChild = pMain->MDIGetActive();
		pChild->SetWindowTextW(sFile.c_str());
		CPolylineViewerView* pView = dynamic_cast<CPolylineViewerView*>(pChild->GetActiveView());
		CPolylineViewerDoc* pDoc = pView->GetDocument();
		pDoc->m_bOnlyExistFlag = false;
        pDoc->SetPathName(sPathName);

		// ポリラインファイルのロード
		if(!pDoc->LoadNewRoadPolyline((LPCTSTR)sPathName))
		{
			AfxMessageBox(_T("ファイルの読み込みに失敗しました。\r\n形式を再度確認して下さい。"));
			pChild->CloseWindow();
		}
        pView->Fit();
		pView->Invalidate();

		if (++iFileCount >= MAX_FILE) {
			::AfxMessageBox(_T("同時に読み込めるファイル数は200までです"));
			break;
		}
	}

    if (bFoundSkipFile) {
        ::AfxMessageBox(_T("読み込み済みのファイルが見つかりました。\r\n読み込み済みのファイルをスキップしました。"));
    }

	RegistryUtil::Save(L"File", L"LastNewPolylineFile", sPathName);
}

void CPolylineViewerApp::OnUpdateNewPolylineOpen(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(true);
}

void CPolylineViewerApp::OnExistPolylineOpen()
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    CString sPathName;
	std::wstring sFile;

    //ファイルダイアログで対象のパスを選択
    {
	    RegistryUtil::Load(L"File", L"LastExistPolylineFile", sPathName);

        std::wstring sDir;
        if (!sPathName.IsEmpty()) {
            SplitFileName((LPCTSTR)sPathName, sDir, sFile);
        }

	    // ファイルパスの指定
        CFileDialog dlg( TRUE,	L"*.txt",	sFile.c_str(),
		    OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		    L"Text Files(*.txt)|*.txt|All Files(*.*)|*.*||",
		    NULL,	0,	TRUE
	    );
        OPENFILENAME& ofn = dlg.GetOFN();
        ofn.lpstrInitialDir = sDir.c_str();
        CString sTitle(_T("Choose Exist Road Polyline file"));
        ofn.lpstrTitle = sTitle;

	    if (dlg.DoModal() != IDOK ){
            return;
        }

	    sPathName = dlg.GetPathName();	// パス付きのファイル名
		if (!sPathName.IsEmpty()) {
            SplitFileName((LPCTSTR)sPathName, sDir, sFile);
        }

	    RegistryUtil::Save(L"File", L"LastExistPolylineFile", sPathName);
    }

    std::set<std::wstring> setPath;
    DocumentWalk(GetLoadedDocumentPath, &setPath);
    if(setPath.find(ToLower((LPCTSTR)sPathName)) != setPath.end()) {
        ::AfxMessageBox(_T("このファイルはすでに新規道路として読み込み済みです"));
        return;
    }

	CWinApp::OnFileNew();
	
	// View,Docの取得
	CMainFrame* pMain = static_cast<CMainFrame*>((AfxGetMainWnd()));
	CMDIChildWnd* pChild = pMain->MDIGetActive();
	sFile += L" (既存道路)";
	pChild->SetWindowTextW(sFile.c_str());
	CPolylineViewerView* pView = dynamic_cast<CPolylineViewerView*>(pChild->GetActiveView());
	CPolylineViewerDoc* pDoc = pView->GetDocument();
	pDoc->m_bOnlyExistFlag = true;
    pDoc->SetPathName(sPathName);

	// ポリラインファイルのロード
	if(!pDoc->LoadExistRoadPolyline((LPCTSTR)sPathName))
	{
		AfxMessageBox(_T("ファイルの読み込みに失敗しました。\r\n形式を再度確認して下さい。"));
		pChild->CloseWindow();
	}

    //全てのViewに対してFit、Invalidateを行う
	ViewWalk(FitAndInvalidate, NULL);
}

void CPolylineViewerApp::OnUpdateExistPolylineOpen(CCmdUI *pCmdUI)
{
    //既存道路だけのドキュメントがある場合、開けない
    bool bFoundOnlyExistRoadDoc = false;
	DocumentWalk(FindOnlyExistRoadDoc, &bFoundOnlyExistRoadDoc);
    pCmdUI->Enable(!bFoundOnlyExistRoadDoc);
}

void CPolylineViewerApp::InvalidateAllView()
{
	DocumentWalk(&InvalidateAllViewFunc, NULL);
}

struct DocWalkFroViewWarkParam {
	ViewWorkFunction* func;
	void* pParam;
};

void DocumentWorkForViewWalk(CDocument* pDoc, void* pParam)
{
	DocWalkFroViewWarkParam* pWalkParam = (DocWalkFroViewWarkParam*)pParam;
	POSITION posView = pDoc->GetFirstViewPosition();
	while (posView)
	{
		CView* pView = pDoc->GetNextView(posView);
		if (pView)
		{
			pWalkParam->func(pView, pWalkParam->pParam);
		}
	}
}

void CPolylineViewerApp::ViewWalk(ViewWorkFunction func, void* pParam)
{
	DocWalkFroViewWarkParam param;
	param.func = func;
	param.pParam = pParam;
	DocumentWalk(DocumentWorkForViewWalk, &param);
}

void CPolylineViewerApp::DocumentWalk(DocumentWorkFunction func, void* pParam)
{
	//全てのViewに対してmatrixの上書き、Invalidateを行う
    POSITION posTempate = GetFirstDocTemplatePosition();
    while (posTempate) 
    {
        CDocTemplate* pTemplate = (CDocTemplate*)GetNextDocTemplate(posTempate);
        POSITION posDoc = pTemplate->GetFirstDocPosition();
        while (posDoc)
        {
            CDocument* pDoc = pTemplate->GetNextDoc(posDoc);
            if (pDoc)
            {
				func(pDoc, pParam);
            }
        }
    }
}

CPolylineViewerView* CPolylineViewerApp::GetActivePolylineView() const
{
	CMainFrame* pMain = static_cast<CMainFrame*>((AfxGetMainWnd()));
	if (!pMain) {
		return NULL;
	}
	CMDIChildWnd* pChild = pMain->MDIGetActive();
	if (!pChild) {
		return NULL;
	}
	return dynamic_cast<CPolylineViewerView*>(pChild->GetActiveView());
}

CPolylineViewerDoc* CPolylineViewerApp::GetActivePolylineDoc() const
{
	CPolylineViewerView* pView = GetActivePolylineView();
	if (!pView) {
		return NULL;
	}
	return pView->GetDocument();
}

CPolylineViewerView* GetActivePolylineView()
{
	return theApp.GetActivePolylineView();
}

CPolylineViewerDoc* GetActivePolylineDoc()
{
	return theApp.GetActivePolylineDoc();
}


void CPolylineViewerApp::OnAppSetting()
{
    AppSettingDialog dlg;
    if (IDOK != dlg.DoModal()) {
        return;
    }
	InvalidateAllView();
}


void CPolylineViewerApp::OnUpdateAppSetting(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(true);
}


void CPolylineViewerApp::OnBmpOutputAll()
{
    //フォルダ選択ダイアログでフォルダを指定
    std::wstring sDir = L"c:\\temp";

    ViewWalk(SaveBmp, &sDir);
}


void CPolylineViewerApp::OnUpdateBmpOutputAll(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(GetActivePolylineView() != NULL);
}


