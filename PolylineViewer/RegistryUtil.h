#pragma once

#include <string>
#include <vector>
#include <list>
#include <set>

/**
 * @brief レジストリへの書き込み、読み込みを行う関数群
 *
 */
class RegistryUtil
{
public:
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, bool& bData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, int& iData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, UINT& iData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, long& iData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, DWORD& iData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, double& dData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, CString& sData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, std::wstring& sData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, std::list<int>& listData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, std::list<std::wstring>& listData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, std::set<int>& setData);
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, std::vector<std::wstring>& arrayData);
    template <class T>
    static bool LoadEnum(LPCTSTR szCategory, LPCTSTR szName, T& data) {
        int i;
        bool bReturn = Load(szCategory, szName, i);
        if (bReturn) {
            data = static_cast<T>(i);
        }
        return bReturn;
    }

    static void Save(LPCTSTR szCategory, LPCTSTR szName, bool bData);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, int iData);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, UINT iData);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, long iData);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, DWORD iData);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, double dData);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, const CString& sData);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, const std::wstring& sData);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, const std::list<int>& listData);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, const std::list<std::wstring>& arrayData);   //空文字列は保存できない事に注意
    static void Save(LPCTSTR szCategory, LPCTSTR szName, const std::set<int>& setData);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, const std::vector<std::wstring>& listData); //空文字列は保存できない事に注意
    static void Save(LPCTSTR szCategory, LPCTSTR szName, LPCTSTR sData);

    static void Clear(LPCTSTR szCategory, LPCTSTR szName);
    static void Clear(LPCTSTR szCategory);

	static void SetBase(std::wstring& base);
	static std::wstring GetBase();

private:
    static bool Load(LPCTSTR szCategory, LPCTSTR szName, DWORD type, LPBYTE& pData, DWORD& iDataSize);
    static void Save(LPCTSTR szCategory, LPCTSTR szName, DWORD type, LPBYTE pData, DWORD iDataSize);

private:
	static std::wstring m_wsRegistryBase;

};
