#pragma once

#include <vector>

class ColorTableFile
{
public:
    ColorTableFile(void);
    virtual ~ColorTableFile(void);

public:
    bool Load(LPCTSTR szPath);
    COLORREF GetColor(size_t index);

private:
    std::vector<COLORREF> m_arrayColor; //色テーブル(読み込んだ順に入っている)

};

