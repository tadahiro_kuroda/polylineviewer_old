#pragma once
// #include <boost/filesystem/path.hpp>
#include "stdafx.h"
#include <vector>

namespace pv{

	/** @brief 点. */
	struct Point
	{
		Point() : m_x(0), m_y(0) {}
		Point(double x, double y) : m_x(x), m_y(y) {}
		double m_x;     ///< X座標
		double m_y;     ///< Y座標
	};

	/** @brief 矩形 */
	struct Rect
	{
	public:
        Rect()
            : m_min(0,0)
            , m_max(0,0)
            , m_empty(true){}
        Rect(const Point& min, const Point& max)
            : m_min(min)
            , m_max(max)
            , m_empty(false){}
    public:
        void Union(const Rect& other);
        void Union(const Point& pt);
        double GetWidth() const {return m_max.m_x - m_min.m_x;}
        double GetHeight() const {return m_max.m_y - m_min.m_y;}
        Point GetCenter() const {return pv::Point((m_min.m_x + m_max.m_x) / 2.0, (m_min.m_y + m_max.m_y) / 2.0);}

    public:
        Point m_min, m_max;
        bool m_empty;
	};

	/** @brief ポリライン. */
	class Polyline
	{
	public:
		std::string m_name;        ///< ポリライン名
		std::vector<Point> m_points;    ///< 構成点列

	public:
		Rect BoundingBox() const;

	public:
		void MoveRight(double move);
		static void MoveRight(double move, std::vector<Point>& polyline);

	public:
		static bool Load(const std::wstring& path, std::vector<Polyline>& polylines);
		static void Save(const std::wstring& path, const std::vector<Polyline>& polylines);
	};
}

void GetBoundingBox(const std::vector<pv::Polyline>& polylines, pv::Rect& bound);

std::ostream& operator<<(std::ostream& os, const pv::Polyline& polyline);
std::istream& operator>>(std::istream& is, pv::Polyline& polyline);
