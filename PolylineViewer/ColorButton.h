#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


/**
 * @brief 色設定ボタンコントロール
 *
 */
class ColorButton : public CButton
{
//コンストラクタ、デストラクタ
public:
    ColorButton(COLORREF color=RGB(192,192,192));
    ~ColorButton(void);

//アトリビュート
    void SetColor(COLORREF color);
	COLORREF GetColor();

    /** @brief ボタンに設定された色を取得する
     *
     *  @return ボタンに設定された色
     */
    COLORREF GetColor() const { return m_Color; }

// オーバーライド
protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT);
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(ColorButton)
	//}}AFX_VIRTUAL

private:
	void DrawFrame(CDC *pDC,const CRect &rect,BOOL bPushed);
	// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(ColorButton)
		// メモ - ClassWizard はこの位置にメンバ関数を追加または削除します。
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	COLORREF m_Color;       ///< ボタンの保持する色
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。
