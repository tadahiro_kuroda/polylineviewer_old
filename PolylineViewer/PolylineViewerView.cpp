
// PolylineViewerView.cpp : CPolylineViewerView クラスの実装
//

#include "stdafx.h"
#include <windows.h>
// SHARED_HANDLERS は、プレビュー、サムネイル、および検索フィルター ハンドラーを実装している ATL プロジェクトで定義でき、
// そのプロジェクトとのドキュメント コードの共有を可能にします。
#ifndef SHARED_HANDLERS
#include "PolylineViewer.h"
#endif

#include "AppSetting.h"
#include "ColorTableFile.h"
#include "MainFrm.h"
#include "PolylineViewerDoc.h"
#include "PolylineViewerView.h"
#include "RegistryUtil.h"
#include "Utils.h"

#define SECTION         L"PolylineViewerView"
#define ALL_VIEW_MODE   L"AllViewMode"
#define SHOW_NAME       L"ShowName"
#define SHOW_POINT      L"ShowPoint"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define USE_DOUBLE_BUFFER //ダブルバッファの使用

const double ZOOM_SCALE = 1.05;

// static 変数
Matrix CPolylineViewerView::s_allTransMatrix(1,0,0, 0,-1,0, 0,0,1);
bool CPolylineViewerView::s_bAllViewMode = false;
bool CPolylineViewerView::s_bShowName = true;
bool CPolylineViewerView::s_bShowPoint = true;

// CPolylineViewerView

IMPLEMENT_DYNCREATE(CPolylineViewerView, CView)

BEGIN_MESSAGE_MAP(CPolylineViewerView, CView)
	// 標準印刷コマンド
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CPolylineViewerView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
    ON_COMMAND(ID_FIT, &CPolylineViewerView::OnFit)
    ON_UPDATE_COMMAND_UI(ID_FIT, &CPolylineViewerView::OnUpdateFit)
    ON_COMMAND(ID_VIEW_ZOOM_OUT, &CPolylineViewerView::OnViewZoomOut)
    ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_OUT, &CPolylineViewerView::OnUpdateViewZoomOut)
    ON_COMMAND(ID_VIEW_ZOOM_IN, &CPolylineViewerView::OnViewZoomIn)
    ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_IN, &CPolylineViewerView::OnUpdateViewZoomIn)
    ON_WM_LBUTTONDOWN()
    ON_WM_LBUTTONUP()
    ON_WM_MOUSEMOVE()
    ON_WM_MOUSEWHEEL()
    ON_COMMAND_RANGE(ID_1PX_UP, ID_SCREEN_SIZE_RIGHT, &CPolylineViewerView::OnPan)
    ON_UPDATE_COMMAND_UI_RANGE(ID_1PX_UP, ID_SCREEN_SIZE_RIGHT, &CPolylineViewerView::OnUpdatePan)
    ON_WM_ERASEBKGND()
    ON_COMMAND(ID_SHOW_HIDE_ROAD_NAME, &CPolylineViewerView::OnShowHideRoadName)
    ON_UPDATE_COMMAND_UI(ID_SHOW_HIDE_ROAD_NAME, &CPolylineViewerView::OnUpdateShowHideRoadName)
    ON_COMMAND(ID_FIT_ALL_VIEW, &CPolylineViewerView::OnFitAllView)
    ON_UPDATE_COMMAND_UI(ID_FIT_ALL_VIEW, &CPolylineViewerView::OnUpdateFitAllView)
    ON_COMMAND(ID_CONTROL_ALL_VIEW, &CPolylineViewerView::OnControlAllView)
    ON_UPDATE_COMMAND_UI(ID_CONTROL_ALL_VIEW, &CPolylineViewerView::OnUpdateControlAllView)
    ON_WM_SIZE()
	ON_COMMAND(ID_BMP_OUTPUT, &CPolylineViewerView::OnBmpOutput)
    ON_COMMAND(ID_SHOW_HIDE_POINT, &CPolylineViewerView::OnShowHidePoint)
    ON_UPDATE_COMMAND_UI(ID_SHOW_HIDE_POINT, &CPolylineViewerView::OnUpdateShowHidePoint)
END_MESSAGE_MAP()

// CPolylineViewerView コンストラクション/デストラクション

CPolylineViewerView::CPolylineViewerView()
    : m_bLButtonDown(false)
    , m_sizeOld(0,0)
{
    m_mTransMatrix.setIdentity();
    m_mTransMatrix.m11 = -1;
}

CPolylineViewerView::~CPolylineViewerView()
{
}

BOOL CPolylineViewerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: この位置で CREATESTRUCT cs を修正して Window クラスまたはスタイルを
	//  修正してください。

	return CView::PreCreateWindow(cs);
}

inline void DrawTextAtCenter(const std::string& s, std::shared_ptr<Gdiplus::Graphics> pGraphics, const Gdiplus::PointF& ptCenter, const Gdiplus::Font& myFont, const Gdiplus::SolidBrush& brushText, const Gdiplus::SolidBrush& brushBack, const Gdiplus::StringFormat& format)
{
    std::wstring ws;
    ConvertMBS2Unicode(s, ws);
    Gdiplus::RectF rectSize;
    pGraphics->MeasureString(ws.c_str(), -1, &myFont, Gdiplus::PointF(0, 0), &rectSize);

    Gdiplus::RectF rectText(ptCenter.X - rectSize.Width / 2.0f, ptCenter.Y - rectSize.Height / 2.0f, rectSize.Width, rectSize.Height);
    pGraphics->FillRectangle(&brushBack, rectText);
    pGraphics->DrawString(ws.c_str(), -1, &myFont, Gdiplus::PointF(rectText.X, rectText.Y), &format, &brushText);
}

inline Gdiplus::Color ColorrefToGdiColor(COLORREF color)
{
    return Gdiplus::Color(255, GetRValue(color), GetGValue(color), GetBValue(color));
}

inline void DrawPoint(std::shared_ptr<Gdiplus::Graphics> pGraphics, const Gdiplus::PointF& pt)
{
    const Gdiplus::REAL CIRCLE_RADIUS = 5.0f;   //FIX_ME 実際には線幅+2にする
    Gdiplus::RectF rect;
    rect.X      = pt.X - CIRCLE_RADIUS;
    rect.Y      = pt.Y - CIRCLE_RADIUS;
    rect.Width  = CIRCLE_RADIUS * 2;
    rect.Height = CIRCLE_RADIUS * 2;
    Gdiplus::SolidBrush brush(Gdiplus::Color(255, 255, 0, 0));  //FIX_ME とりあえず赤、色を設定できるように
    pGraphics->FillEllipse(&brush, rect);
}

#ifdef USE_DOUBLE_BUFFER
//ダブルバッファリング
class DoubleBuffer
{
public:
    DoubleBuffer(const CRect& rect, CDC* pDC)
        : m_rect(rect)
        , m_pDC(pDC)
    {
		memDC.CreateCompatibleDC(m_pDC);

		//仮想デバイスコンテキスト用ビットマップ作成
		memBmp.CreateCompatibleBitmap(m_pDC, m_rect.Width(), m_rect.Height());

		//仮想デバイスコンテキストにビットマップ設定
		pOldBmp = memDC.SelectObject(&memBmp);

		//元のブラシをoldbrに保持
		CBrush brush(RGB(255,255,255));
		pBrushOld = memDC.SelectObject(&brush);
		//ブラシを白色にする
		memDC.SelectObject(&brush);
		//背景を白にする
		memDC.Rectangle(&rect);
    }
    ~DoubleBuffer()
    {
		//描画
		BitBlt(m_pDC->GetSafeHdc(), 0, 0, m_rect.Width(), m_rect.Height(), memDC.GetSafeHdc(), 0, 0, SRCCOPY);

    	//元のブラシに戻す
	    memDC.SelectObject(pBrushOld);
	    //仮想デバイスコンテキストのビットマップを初期化
	    memDC.SelectObject(pOldBmp);
	    //仮想デバイスコンテキストのビットマップを廃棄
	    memBmp.DeleteObject();
    }
    
public:
    CDC& GetDC() {return memDC;}

private:
    const CRect& m_rect;
    CDC* m_pDC;
	CDC memDC;
	CBitmap memBmp;
    CBitmap* pOldBmp;
    CBrush* pBrushOld;
};

#endif //USE_DOUBLE_BUFFER


void CPolylineViewerView::OnDraw(CDC* pDC)
{
	Draw(pDC);
}

// CPolylineViewerView 描画
void CPolylineViewerView::Draw(CDC* pDC)
{
#ifdef USE_DOUBLE_BUFFER
    //ダブルバッファリング用の準備
    CRect rect;
    GetClientRect(&rect);
    DoubleBuffer doubleBuffer(rect, pDC);
#endif // USE_DOUBLE_BUFFER

    std::shared_ptr<AppSetting> pSetting = theApp.GetAppSetting();
	CPolylineViewerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

    std::shared_ptr<Gdiplus::Graphics> pGraphics
#ifdef USE_DOUBLE_BUFFER
        = std::shared_ptr<Gdiplus::Graphics>(Gdiplus::Graphics::FromHDC(doubleBuffer.GetDC().GetSafeHdc()));
#else
        = std::shared_ptr<Gdiplus::Graphics>(Gdiplus::Graphics::FromHDC(pDC->GetSafeHdc()));
#endif

    typedef std::vector<Gdiplus::PointF> GdiPolyline;
    typedef std::vector<GdiPolyline> GdiPolylineArray;
    GdiPolylineArray exist_polylines, new_road_polyines;
    {
	    // 既存道路描画
        exist_polylines.resize(CPolylineViewerDoc::s_vExistPolyline.size());
        for (size_t i = 0; i < CPolylineViewerDoc::s_vExistPolyline.size(); ++i) {
            PolylinePointToClient(CPolylineViewerDoc::s_vExistPolyline[i].m_points, exist_polylines[i]);
	    }

	    // 新規道路描画
        new_road_polyines.resize(pDoc->m_vNewPolyline.size());
        for (size_t i = 0; i < pDoc->m_vNewPolyline.size(); ++i) {
            PolylinePointToClient(pDoc->m_vNewPolyline[i].m_points, new_road_polyines[i]);
	    }
    }
    //ポリライン
    {
	    // 既存道路描画
        {
            Gdiplus::Pen pen(ColorrefToGdiColor(pSetting->colorExistPolyline), static_cast<Gdiplus::REAL>(pSetting->iExistPolylineWidth));
	        for(GdiPolylineArray::iterator it = exist_polylines.begin(); it != exist_polylines.end(); it++) {	
                pGraphics->DrawLines(&pen, &(it->at(0)), static_cast<INT>(it->size()));
	        }

        }

	    // 新規道路描画
        for (size_t i = 0; i < new_road_polyines.size(); ++i) {
            COLORREF color;
            if (pSetting->bUseRandomColor) {
                color = theApp.GetColorTable()->GetColor(i);
            } else {
                color = pSetting->colorNewPolyline;
            }
            Gdiplus::Pen pen(ColorrefToGdiColor(color), static_cast<Gdiplus::REAL>(pSetting->iNewPolylineWidth));
    	    const GdiPolyline& polyline = new_road_polyines[i];
            pGraphics->DrawLines(&pen, &polyline[0], static_cast<INT>(polyline.size()));
	    }
    }

    //端点
    if (s_bShowPoint) {
	    // 既存道路描画
        Gdiplus::Pen pen(ColorrefToGdiColor(pSetting->colorExistPolyline), static_cast<Gdiplus::REAL>(pSetting->iExistPolylineWidth));
	    for(GdiPolylineArray::iterator it = exist_polylines.begin(); it != exist_polylines.end(); it++) {	
            DrawPoint(pGraphics, it->front());
            DrawPoint(pGraphics, it->back());
	    }

	    // 新規道路描画
	    for(GdiPolylineArray::iterator it = new_road_polyines.begin(); it != new_road_polyines.end(); it++) {	
            DrawPoint(pGraphics, it->front());
            DrawPoint(pGraphics, it->back());
	    }
    }

    //テキスト
    if (s_bShowName) {
        Gdiplus::Font myFont(L"Arial", 10.0f);
        Gdiplus::SolidBrush brushText(Gdiplus::Color(255, 0, 0, 0));
        Gdiplus::SolidBrush brushBack(Gdiplus::Color(128, 200, 200, 255));
        Gdiplus::StringFormat format;
        format.SetAlignment(Gdiplus::StringAlignmentNear);

	    // 既存道路描画
        if (pDoc->m_bOnlyExistFlag) {
            Gdiplus::Pen pen(ColorrefToGdiColor(pSetting->colorExistPolyline), static_cast<Gdiplus::REAL>(pSetting->iExistPolylineWidth));
            for (size_t i = 0; i < CPolylineViewerDoc::s_vExistPolyline.size(); ++i) {
                DrawTextAtCenter(CPolylineViewerDoc::s_vExistPolyline[i].m_name, pGraphics, GetPolylineDiahyoPoint(exist_polylines[i]), myFont, brushText, brushBack, format);
	        }
        }

	    // 新規道路描画
        for (size_t i = 0; i < pDoc->m_vNewPolyline.size(); ++i) {
    	    const GdiPolyline& polyline = new_road_polyines[i];
            DrawTextAtCenter(pDoc->m_vNewPolyline[i].m_name, pGraphics, GetPolylineDiahyoPoint(new_road_polyines[i]), myFont, brushText, brushBack, format);
	    }
    }
}

inline pv::Point TransformByMatrix(const Matrix& matrix, const pv::Point& pt)
{
    return pv::Point(
	    pt.m_x * matrix.m00 + pt.m_y * matrix.m01 + matrix.m02,
        pt.m_x * matrix.m10 + pt.m_y * matrix.m11 + matrix.m12);
}

Gdiplus::PointF CPolylineViewerView::PolylinePointToClient(const pv::Point& pt) const
{
    pv::Point temp = TransformByMatrix(GetTargetMatrix(), pt);
    return Gdiplus::PointF(static_cast<Gdiplus::REAL>(temp.m_x), static_cast<Gdiplus::REAL>(temp.m_y));
}

void CPolylineViewerView::PolylinePointToClient(const std::vector<pv::Point>& pointsPolyline, std::vector<Gdiplus::PointF>& pointsClient) const
{
    pointsClient.resize(pointsPolyline.size());
    for (size_t i = 0; i < pointsPolyline.size(); ++i) {
        pointsClient[i] = PolylinePointToClient(pointsPolyline[i]);
    }
}

pv::Point CPolylineViewerView::ClientPointToPolyline(const Gdiplus::PointF& pt) const
{
    Matrix mInvert;
    mInvert.invert(GetTargetMatrix());
    return TransformByMatrix(mInvert, pv::Point(pt.X, pt.Y));
}

void CPolylineViewerView::ClientPointToPolyline(const std::vector<Gdiplus::PointF>& pointsClient, std::vector<pv::Point>& pointsPolyline) const
{
    pointsPolyline.resize(pointsClient.size());
    for (size_t i = 0; i < pointsClient.size(); ++i) {
        pointsPolyline[i] = ClientPointToPolyline(pointsClient[i]);
    }
}

// PolylineのバウンディングボックスとViewのサイズから変換行列を設定する
void CPolylineViewerView::Fit()
{
	CPolylineViewerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc || (pDoc->m_vNewPolyline.empty() && CPolylineViewerDoc::s_vExistPolyline.empty())) {
		return;
    }

	// ウィンドウサイズ取得
	CRect rectView;
	GetClientRect(&rectView);

	// バウンディングボックス取得
	pv::Rect boundingBox;
	// 全てのポリラインが収まるバウンディングボックスを求める
	for(std::vector<pv::Polyline>::iterator it = pDoc->m_vNewPolyline.begin(); it != pDoc->m_vNewPolyline.end(); it++ )
	{
        boundingBox.Union(it->BoundingBox());
	}
	for(std::vector<pv::Polyline>::iterator it = CPolylineViewerDoc::s_vExistPolyline.begin(); it != CPolylineViewerDoc::s_vExistPolyline.end(); it++ )
	{
        boundingBox.Union(it->BoundingBox());
	}

	// アフィン変換行列設定
	// 計算したバウンディングボックスのx軸がViewに収まるように変換

    //幅、高さの小さい方の上下左右に5％をマージンとして設定する
    double clinet_margin;
    if (rectView.Width() < rectView.Height()) {
        clinet_margin = rectView.Width() / 20;
    } else {
        clinet_margin = rectView.Height() / 20;
    }

    //Viewの中で実際に表示する範囲
    double show_size_x = rectView.Width() - clinet_margin * 2;
	double show_size_y = rectView.Height() - clinet_margin * 2;
	
	// 座標の縮小率を計算
    double xScale = show_size_x / boundingBox.GetWidth(); // x軸方向の縮小率
    double yScale = show_size_y / boundingBox.GetHeight(); // y軸方向の縮小率
	// 縮小率の大きい(値の小さい)ほうを使用
    double scale = std::min(xScale, yScale);

    //原点を中心にするように移動
    Matrix matrixTranslate1;
    matrixTranslate1.setIdentity();
    pv::Point ptCenter = boundingBox.GetCenter();
    matrixTranslate1.m02 = -ptCenter.m_x;
    matrixTranslate1.m12 = -ptCenter.m_y;

    //スケールの調整
    Matrix matrixScale;
    matrixScale.setIdentity();
    matrixScale.m00 = matrixScale.m11 = scale;

    //上下の反転
    Matrix matrixVInvert;
    matrixVInvert.setIdentity();
    matrixVInvert.m11 = -1;

    //Viewの中心へ移動
    Matrix matrixTranslate2;
    matrixTranslate2.setIdentity();
    matrixTranslate2.m02 = rectView.left + rectView.Width() / 2.0;
    matrixTranslate2.m12 = rectView.top + rectView.Height() / 2.0;

    GetTargetMatrix() = matrixTranslate2;
    GetTargetMatrix().mul(matrixVInvert);
    GetTargetMatrix().mul(matrixScale);
    GetTargetMatrix().mul(matrixTranslate1);
}

// CPolylineViewerView 印刷


void CPolylineViewerView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CPolylineViewerView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 既定の印刷準備
	return DoPreparePrinting(pInfo);
}

void CPolylineViewerView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 印刷前の特別な初期化処理を追加してください。
}

void CPolylineViewerView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 印刷後の後処理を追加してください。
}

void CPolylineViewerView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CPolylineViewerView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	//theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CPolylineViewerView 診断

#ifdef _DEBUG
void CPolylineViewerView::AssertValid() const
{
	CView::AssertValid();
}

void CPolylineViewerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CPolylineViewerDoc* CPolylineViewerView::GetDocument() const // デバッグ以外のバージョンはインラインです。
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPolylineViewerDoc)));
	return (CPolylineViewerDoc*)m_pDocument;
}
#endif //_DEBUG


// CPolylineViewerView メッセージ ハンドラー


void CPolylineViewerView::OnFit()
{
    Fit();
    InvalidateView();
}

void CPolylineViewerView::OnUpdateFit(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(true);
}

void CPolylineViewerView::OnLButtonDown(UINT nFlags, CPoint point)
{
    m_bLButtonDown = true;
	m_ptDown = point;
	m_mTransMatrixButtonDown = GetTargetMatrix();
	SetCapture();
	::SetCursor(theApp.LoadStandardCursor(IDC_SIZEALL));
}

void CPolylineViewerView::OnLButtonUp(UINT nFlags, CPoint point)
{
    m_bLButtonDown = false;
	::ReleaseCapture();
}

void CPolylineViewerView::OnMouseMove(UINT nFlags, CPoint point)
{
    if (!m_bLButtonDown) {
        CView::OnMouseMove(nFlags, point);
    }

    //マウスの左ボタンが押し込まれている場合のみ処理を行う
	else
	{
		CSize szMove = point - m_ptDown;
		
		Matrix translate;
		translate.set(1.0, 0.0, szMove.cx,
				      0.0, 1.0, szMove.cy,
				  	  0.0, 0.0,       1.0);

		GetTargetMatrix().mul(translate, m_mTransMatrixButtonDown);
		InvalidateView();
	}

}

BOOL CPolylineViewerView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	if (0 < zDelta)
	{
		OnViewZoomOut();
		
	}
	else if (zDelta < 0)
	{
		OnViewZoomIn();
	} 
    return CView::OnMouseWheel(nFlags, zDelta, pt);
}

inline Gdiplus::PointF CPointToGdiPointF(const CPoint& pt)
{
    return Gdiplus::PointF(static_cast<Gdiplus::REAL>(pt.x), static_cast<Gdiplus::REAL>(pt.y));
}

void CPolylineViewerView::ZoomAtViewCenter(double dScale)
{
	// スクリーン座標の中心座標を求める
	CRect rect;
	GetClientRect(&rect);
	CPoint ptClientCenter(rect.Width() / 2 + rect.left, rect.Height() / 2 + rect.top);

	pv::Point ptCenter =  ClientPointToPolyline(CPointToGdiPointF(ptClientCenter));

	Matrix translate1, scale, translate2;

	translate1.set(1.0, 0.0, -ptCenter.m_x,
				   0.0, 1.0, -ptCenter.m_y,
				   0.0, 0.0,   1.0);
	scale.set(dScale,    0.0, 0.0,
			     0.0, dScale, 0.0,
			     0.0,    0.0, 1.0);
	translate2.set(1.0, 0.0, ptCenter.m_x,
				   0.0, 1.0, ptCenter.m_y,
				   0.0, 0.0,  1.0);

	Matrix& matrix = GetTargetMatrix();
	matrix.mul(translate2);
	matrix.mul(scale);
	matrix.mul(translate1);

	InvalidateView();
}

void CPolylineViewerView::OnViewZoomIn()
{
	ZoomAtViewCenter(ZOOM_SCALE);
}

void CPolylineViewerView::OnUpdateViewZoomIn(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(true);
}

void CPolylineViewerView::OnViewZoomOut()
{
	ZoomAtViewCenter(1.0 / ZOOM_SCALE);
}

void CPolylineViewerView::OnUpdateViewZoomOut(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(true);
}

void CPolylineViewerView::InvalidateView()
{
	if (s_bAllViewMode) {
		theApp.InvalidateAllView();
	} else {
		Invalidate();
	}
}


void CPolylineViewerView::OnPan(UINT id)
{
    CRect rectClient;
    switch (id) {
    case ID_SCREEN_SIZE_UP:
    case ID_SCREEN_SIZE_DOWN:
    case ID_SCREEN_SIZE_LEFT:
    case ID_SCREEN_SIZE_RIGHT:
        GetClientRect(&rectClient);
        break;
    }

    CSize move(0, 0);
    switch (id) {
    case ID_1PX_UP:             move.cx = 0;    move.cy = -1;   break;
    case ID_1PX_DOWN:           move.cx = 0;    move.cy = 1;    break;
    case ID_1PX_LEFT:           move.cx = -1;   move.cy = 0;    break;
    case ID_1PX_RIGHT:          move.cx = 1;    move.cy = 0;    break;
    case ID_10PX_UP:            move.cx = 0;    move.cy = -10;  break;
    case ID_10PX_DOWN:          move.cx = 0;    move.cy = 10;   break;
    case ID_10PX_LEFT:          move.cx = -10;  move.cy = 0;    break;
    case ID_10PX_RIGHT:         move.cx = 10;   move.cy = 0;    break;
    case ID_100PX_UP:           move.cx = 0;    move.cy = -100; break;
    case ID_100PX_DOWN:         move.cx = 0;    move.cy = 100;  break;
    case ID_100PX_LEFT:         move.cx = -100; move.cy = 0;    break;
    case ID_100PX_RIGHT:        move.cx = 100;  move.cy = 0;    break;
    case ID_SCREEN_SIZE_UP:     move.cx = 0;                    move.cy = -rectClient.Height(); break;
    case ID_SCREEN_SIZE_DOWN:   move.cx = 0;                    move.cy = rectClient.Height();  break;
    case ID_SCREEN_SIZE_LEFT:   move.cx = -rectClient.Width();  move.cy = 0;    break;
    case ID_SCREEN_SIZE_RIGHT:  move.cx = rectClient.Width();   move.cy = 0;    break;
    default:
        _ASSERTE(false);
        return;
    }

    //moveのサイズ分だけViewの平行移動の処理
	Matrix translate;
	translate.set(1.0, 0.0, move.cx,
				  0.0, 1.0, move.cy,
				  0.0, 0.0,     1.0);

    Matrix& matrixTarget = GetTargetMatrix();
	matrixTarget.mul(translate, matrixTarget);
	InvalidateView();
  
}


void CPolylineViewerView::OnUpdatePan(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(true);
}

Gdiplus::PointF CPolylineViewerView::GetPolylineDiahyoPoint(const std::vector<Gdiplus::PointF>& points)
{
    if (points.empty()) {
        _ASSERTE(false);
        return Gdiplus::PointF(0, 0);
    }

    size_t nCount = points.size();
    if (nCount % 2 == 1) {
        //奇数の場合
        return points[nCount / 2];
    }

    //偶数の場合
    size_t iPrev = nCount / 2 - 1;
    size_t iNext = nCount / 2;

    return Gdiplus::PointF(
        (points[iPrev].X + points[iNext].X) / 2,
        (points[iPrev].Y + points[iNext].Y) / 2);
}


BOOL CPolylineViewerView::OnEraseBkgnd(CDC* pDC)
{
#ifdef USE_DOUBLE_BUFFER
    return TRUE;
#else
    return CView::OnEraseBkgnd(pDC);
#endif
}


void CPolylineViewerView::OnShowHideRoadName()
{
    s_bShowName = !s_bShowName;
    theApp.InvalidateAllView();
}


void CPolylineViewerView::OnUpdateShowHideRoadName(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(s_bShowName);
}

namespace {
	void SetViewMatrix(CView* pView, void* pParam)
	{
		const Matrix* pMatrix = static_cast<Matrix*>(pParam);
		CPolylineViewerView* pThisView = dynamic_cast<CPolylineViewerView*>(pView);
		if (!pThisView) {
			_ASSERTE(false);
			return;
		}
		pThisView->SetMatrix(*pMatrix);
	}
}

void CPolylineViewerView::OnFitAllView()
{
    //全てのViewに対してmatrixの上書き、Invalidateを行う
    Matrix matrix = GetMatrix();
    theApp.ViewWalk(SetViewMatrix, &matrix);
    theApp.InvalidateAllView();
}


void CPolylineViewerView::OnUpdateFitAllView(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(!s_bAllViewMode);
}


void CPolylineViewerView::OnControlAllView()
{
    s_bAllViewMode = !s_bAllViewMode;

	if(s_bAllViewMode){
        s_allTransMatrix = m_mTransMatrix;
	} else{
		//全てのViewに対してmatrixの上書きを行う
        theApp.ViewWalk(SetViewMatrix, &s_allTransMatrix);
	}
	theApp.InvalidateAllView();
}


void CPolylineViewerView::OnUpdateControlAllView(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(s_bAllViewMode);
}


void CPolylineViewerView::OnSize(UINT nType, int cx, int cy)
{
    if (m_sizeOld.cx != 0 && m_sizeOld.cy != 0
        && cx != 0 && cy != 0) {
        //サイズ調整
        double dTransOldX = m_sizeOld.cx / 2.0;
        double dTransOldY = m_sizeOld.cy / 2.0;
		
		Matrix mTranslateOld(1, 0, -m_sizeOld.cx / 2.0,
				             0, 1, -m_sizeOld.cy / 2.0,
				  	         0, 0,                   1);

        double scale = (double)std::min(cx, cy)
                     / (double)std::min(m_sizeOld.cx, m_sizeOld.cy);
        Matrix mScale(scale,    0, 0,
				         0, scale, 0,
				  	     0,     0, 1);

        Matrix mTranslateNew(1, 0, cx / 2.0,
				             0, 1, cy / 2.0,
				  	         0, 0,        1);

        Matrix m;
        m.setIdentity();
        m.mul(mTranslateNew);
        m.mul(mScale);
        m.mul(mTranslateOld);
		GetTargetMatrix().mul(m, GetTargetMatrix());
		InvalidateView();
        
    }
    m_sizeOld.cx = cx;
    m_sizeOld.cy = cy;
    CView::OnSize(nType, cx, cy);
}


void CPolylineViewerView::OnBmpOutput()
{
	CString sPathName;
	RegistryUtil::Load(L"File", L"LastNewPolylineFile", sPathName); //FIX_ME 変更確認

    std::wstring sDir, sFile;
    if (!sPathName.IsEmpty()) {
        SplitFileName((LPCTSTR)sPathName, sDir, sFile);
    }

	//アクティブなView,Docの獲得
	//CMainFrame* pMain = static_cast<CMainFrame*>((AfxGetMainWnd()));
	//CMDIChildWnd* pChild = pMain->MDIGetActive();
	//CPolylineViewerView* pView = dynamic_cast<CPolylineViewerView*>(pChild->GetActiveView());
	CPolylineViewerDoc* pDoc = dynamic_cast<CPolylineViewerDoc*>(this->GetDocument());
	
    std::wstring sExt, sBody;
	SplitFileExtension((LPCTSTR)pDoc->GetTitle(), sBody, sExt);

	CFileDialog dlg(FALSE, L"*.bmp", sBody.c_str(),
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT ,
		L"Bitmap File(*.bmp)|*.bmp|All Files(*.*)|*.*||",
		NULL, 0, TRUE
	);

	OPENFILENAME& ofn = dlg.GetOFN();
	ofn.lpstrInitialDir = sDir.c_str();
	CString sTitle(_T("Choose Save Bitmap file"));
	ofn.lpstrTitle = sTitle;

	if (dlg.DoModal() != IDOK){
		return;
	}

    std::wstring sPath = (LPCTSTR)dlg.GetPathName();

	//BMP出力処理
	BmpOutput(sPath);
	
	RegistryUtil::Save(L"File", L"LastNewPolylineFile", sPath); //FIX_ME 変更確認

}

void CPolylineViewerView::BmpOutput(const std::wstring& sPath)
{
	CClientDC dc(this);
    CBitmap bmpForSave;
    CDC cdcForSave;
    CRect r;
    GetClientRect(&r);

    bmpForSave.CreateCompatibleBitmap(&dc, r.Width(), r.Height());
    cdcForSave.CreateCompatibleDC(&dc);
    cdcForSave.SelectObject(bmpForSave);
	Draw(&cdcForSave);

	CImage image;
	image.Attach(bmpForSave);
	image.Save(sPath.c_str());
	image.Detach();
}

void CPolylineViewerView::OnShowHidePoint()
{
    s_bShowPoint = !s_bShowPoint;
    theApp.InvalidateAllView();
}

void CPolylineViewerView::OnUpdateShowHidePoint(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(s_bShowPoint);
}

void CPolylineViewerView::SaveFlag()
{
    RegistryUtil::Save(SECTION, ALL_VIEW_MODE, s_bAllViewMode);
    RegistryUtil::Save(SECTION, SHOW_NAME, s_bShowName);
    RegistryUtil::Save(SECTION, SHOW_POINT, s_bShowPoint);
}

void CPolylineViewerView::LoadFlag()
{
    RegistryUtil::Load(SECTION, ALL_VIEW_MODE, s_bAllViewMode);
    RegistryUtil::Load(SECTION, SHOW_NAME, s_bShowName);
    RegistryUtil::Load(SECTION, SHOW_POINT, s_bShowPoint);
}
