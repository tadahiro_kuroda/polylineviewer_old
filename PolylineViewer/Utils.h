#pragma once

#include <string>

bool ConvertMBS2Unicode(const std::string& s, std::wstring& sResult);
bool ConvertUnicode2MBS(const std::wstring& s, std::string& sResult);
bool ConvertUTF82Unicode(const std::string& s, std::wstring& sResult);
bool ConvertUnicode2UTF8(const std::wstring& s, std::string& sResult);
bool ConvertJisToSJis(const std::string& jis, std::string& sjis);
bool ConvertSJisToJis(const std::string& sjis, std::string& jis);

bool IsFileExist(const char* szFile, bool bExistDirCheck = true);
bool IsFileExist(const wchar_t* szFile, bool bExistDirCheck = true);
bool IsFileExist(const std::string& sFile, bool bExistDirCheck = true);
bool IsFileExist(const std::wstring& sFile, bool bExistDirCheck = true);
void SplitFileName(const std::wstring& sFileNameWithPath, std::wstring& sDir, std::wstring& sFile);
void SplitFileName(const std::string& sFileNameWithPath, std::string& sDir, std::string& sFile);
bool SplitFileExtension(const std::wstring& sFileName, std::wstring& sBody, std::wstring& sExt);
std::wstring ConnectPath(const std::wstring& sDir, const std::wstring& sPath);
std::wstring ConnectPath(const std::wstring& sBaseDir, const std::wstring& sDir1, const std::wstring& sPath);
std::wstring ConnectPath(const std::wstring& sBaseDir, const std::wstring& sDir1, const std::wstring& sDir2, const std::wstring& sPath);
std::string ConnectPath(const std::string& sDir, const std::string& sPath);
std::string ConnectPath(const std::string& sBaseDir, const std::string& sDir1, const std::string& sPath);
std::string ConnectPath(const std::string& sBaseDir, const std::string& sDir1, const std::string& sDir2, const std::string& sPath);
bool IsFileExtension(const std::wstring& sFileName, const std::wstring& sExt);
std::wstring ChangeFileExtension(const std::wstring& sFileName, const std::wstring& sNewExt);
std::wstring AddExtension(const std::wstring& sFile, const std::wstring& sExt);
bool IsNetworkFile(const std::string& sFullPath);
bool IsNetworkFile(const std::wstring& sFullPath);
bool IsHddFile(const std::wstring& sFullPath);
bool IsFullPath(const std::string& sPath);
bool IsFullPath(const std::wstring& sPath);
bool CreateDirectories(const std::wstring& sDir, std::wstring* psCannotCreateDir = NULL);
std::wstring ToLower(const std::wstring& s);
std::string ToLower(const std::string& s);
bool IsSamePath(const std::wstring& path1, const std::wstring& path2);
std::wstring DoubleToWString(double d);
std::wstring ColorToWString(COLORREF color);
bool IsZeroString(LPCTSTR sz);
std::wstring GetModuleDir();
std::wstring EscapePercentage(const std::wstring& s);
bool RemoveDirectories(const std::wstring& sPath);

size_t LengthNumber(const wchar_t* s);
bool ReadNumber(double& dRead, std::wstring& s);

//SJISの1バイト目のコード?
inline bool IsTwoBytesChar(const char c)
{
    return ('\x81'<=(c)&&(c)<='\x9F') || ('\xE0'<=(c)&&(c)<='\xFC');
}

/**
 * @brief フラグの値に、コンストラクタtrue、デストラクタでfalseを設定するためのユーティリティー
 *
 * @ingroup EPS2PDF2
 *
 */
class FlagHandler
{
public:
    FlagHandler(bool& b) : m_b(b) { m_b = true; }
    ~FlagHandler() { m_b = false; }
private:
    bool& m_b;
};
