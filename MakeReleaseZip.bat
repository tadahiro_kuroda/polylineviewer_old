:プロジェクト名称
set PROJECT_NAME=PolylineViewer
:リリースしたファイルを置く位置
set RELEASE_DIR=\\zserver4\AW\project\道路生成\リリース
:リリースするファイルの一式が入ったファイル位置
set TARGET_DIR=ship

set DATE8=%date:~-10,4%%date:~-5,2%%date:~-2,2%
set ZIP_FILE_NAME=%PROJECT_NAME%_%DATE8%.zip
cd %TARGET_DIR%
"c:\Program Files\7-Zip\7z.exe" a %ZIP_FILE_NAME% *
copy %ZIP_FILE_NAME% %RELEASE_DIR% /Y
del %ZIP_FILE_NAME%
cd ..